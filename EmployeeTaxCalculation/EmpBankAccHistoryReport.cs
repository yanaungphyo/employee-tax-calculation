﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace EmployeeTaxCalculation
{
    public partial class frmEmpBankAccHistoryReport : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlDataAdapter da;
        DataTable dt;
        public frmEmpBankAccHistoryReport()
        {
            InitializeComponent();
        }

        private void EmpBankAccHistoryReport_Load(object sender, EventArgs e)
        {
            string queryEmployee = @"select id, employee_name from Employee where is_deleted=0";
            SqlDataAdapter daEmployee = new SqlDataAdapter(queryEmployee, con);
            DataTable dtEmployee = new DataTable();
            daEmployee.Fill(dtEmployee);
            cmbEmployee.DisplayMember = "employee_name";
            cmbEmployee.ValueMember = "id";
            cmbEmployee.DataSource = dtEmployee;
            this.cmbEmployee.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cmbEmployee.AutoCompleteSource = AutoCompleteSource.ListItems;

            string queryBank = @"select id, bank from Bank where is_deleted=0";
            SqlDataAdapter daBank = new SqlDataAdapter(queryBank, con);
            DataTable dtBank = new DataTable();
            daBank.Fill(dtBank);
            cmbBank.DisplayMember = "bank";
            cmbBank.ValueMember = "id";
            cmbBank.DataSource = dtBank;
            this.cmbBank.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cmbBank.AutoCompleteSource = AutoCompleteSource.ListItems;         
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
                con.Open();
            string query = @"select * from vw_EmpBankAccHistory_Report ";

            if (chkEmployee.Checked == true & chkBank.Checked == true)
            {
                if (cmbEmployee.SelectedItem == null)
                {
                    MessageBox.Show("Please select Employee");
                }
                else if (cmbBank.SelectedItem == null)
                {
                    MessageBox.Show("Please select Bank");
                }
                else
                    query += @" where employee_name='" + cmbEmployee.Text + "' and bank='" + cmbBank.Text + "'";

            }

            else if (chkEmployee.Checked == true)
            {
                if (cmbEmployee.SelectedItem == null)
                {
                    MessageBox.Show("Please select Employee");
                    return;
                }
                else
                    query += "  where employee_name='" + cmbEmployee.Text + "'";
            }

            else if(chkBank.Checked==true)
            {
            if(cmbBank.SelectedItem==null)
            {
                MessageBox.Show("Please select Bank");
                return;
            }
            else
                query += @" where bank='" + cmbBank.Text + "'";
            }
            EmployeeTaxCalculationDataSet ds = new EmployeeTaxCalculationDataSet();

            da = new SqlDataAdapter(query, con);
            da.Fill(ds, "Emp_Bank_Acc_History_Report");
            ReportDataSource dataSource = new ReportDataSource("Emp_Bank_Acc_History", ds.Tables[8]);
            this.rvEmpBankAccHistory.LocalReport.DataSources.Clear();
            this.rvEmpBankAccHistory.LocalReport.DataSources.Add(dataSource);
            this.rvEmpBankAccHistory.LocalReport.Refresh();
            this.rvEmpBankAccHistory.RefreshReport();
            con.Close();
        }

        private void btnShowAll_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
                con.Open();
            string queryShowAll = @"select * from vw_EmpBankAccHistory_Report";
            EmployeeTaxCalculationDataSet ds = new EmployeeTaxCalculationDataSet();

            da = new SqlDataAdapter(queryShowAll, con);
            da.Fill(ds, "Emp_Bank_Acc_History_Report");
            ReportDataSource dataSource = new ReportDataSource("Emp_Bank_Acc_History", ds.Tables[8]);
            this.rvEmpBankAccHistory.LocalReport.DataSources.Clear();
            this.rvEmpBankAccHistory.LocalReport.DataSources.Add(dataSource);
            this.rvEmpBankAccHistory.LocalReport.Refresh();
            this.rvEmpBankAccHistory.RefreshReport();
            con.Close();
        }

       
    }
}
