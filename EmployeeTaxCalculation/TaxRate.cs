﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmTaxRate : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmdTaxRate;
        SqlDataAdapter daTaxRate;
        decimal d;
        
        public frmTaxRate()
        {
            InitializeComponent();
            
            if (!btnTaxRateSave.Enabled)
            {
                btnTaxRateSave.Enabled = true;
            }
            if (btnUpdate.Enabled)
            {
                btnUpdate.Enabled = false;
            }
            
            dgvTaxRate.AutoGenerateColumns = false;            
            DisplayData();
            Clear();
        }
        bool above;
        private void btnTaxRateSave_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
                con.Open();

            if (chkAbove.Checked == true)
            {
                above = true;
            }
            else
            {
                above = false;
                if (!decimal.TryParse(txtMaxTaxableIncome.Text, out d))
                {
                    MessageBox.Show("Please enter Maximum Taxable Income");
                    return;
                }
                else if (Decimal.Parse(txtMaxTaxableIncome.Text) < 0)
                {
                    MessageBox.Show("Please enter correct Maximum Taxable Income");
                    return;
                }
            }

            if (!decimal.TryParse(txtMinTaxableIncome.Text, out d))
            {
                MessageBox.Show("Please enter Minimum Taxable Income");
                return;
            }
            else if (Decimal.Parse(txtMinTaxableIncome.Text) < 0)
            {
                MessageBox.Show("Please enter correct Minimum Taxable Income");
                return;
            }



            if (chkAbove.Checked == true)
            {
                above = true;
            }
            else
            {
                above = false;
                if (!decimal.TryParse(txtMaxTaxableIncome.Text, out d))
                {
                    MessageBox.Show("Please enter Maximum Taxable Income");
                    return;
                }
                else if (Decimal.Parse(txtMaxTaxableIncome.Text) < 0)
                {
                    MessageBox.Show("Please enter correct Maximum Taxable Income");
                    return;
                }
                if (Decimal.Parse(txtMaxTaxableIncome.Text) < Decimal.Parse(txtMinTaxableIncome.Text))
                {
                    MessageBox.Show("minimum is greater than maximum");
                    return;
                }
            }

            if (!decimal.TryParse(txtPercentage.Text, out d))
            {
                MessageBox.Show("Please enter Percentage");
                return;
            }
            else if (Decimal.Parse(txtPercentage.Text) < 0)
            {
                MessageBox.Show("Please enter correct Percentage");
                return;
            }                    
                string queryTaxRate = "INSERT INTO Tax_Rate (min_taxable_income, max_taxable_income, percentage, above, is_deleted, created_date, updated_date) values (@min_taxable_income,@max_taxable_income,@percentage, @above, @is_deleted, @created_date, @updated_date)";
                cmdTaxRate = new SqlCommand(queryTaxRate, con);
                cmdTaxRate.Parameters.AddWithValue("@min_taxable_income", Decimal.Parse(txtMinTaxableIncome.Text));


                if (chkAbove.Checked == true)
                {

                    cmdTaxRate.Parameters.AddWithValue("@max_taxable_income", DBNull.Value);

                }
                else
                {
                    cmdTaxRate.Parameters.AddWithValue("@max_taxable_income", txtMaxTaxableIncome.Text);
                }
                cmdTaxRate.Parameters.AddWithValue("@percentage", Decimal.Parse(txtPercentage.Text));
                cmdTaxRate.Parameters.AddWithValue("@above", above);
                cmdTaxRate.Parameters.AddWithValue("@is_deleted", Boolean.FalseString);
                cmdTaxRate.Parameters.AddWithValue("@created_date", DateTime.Now);
                cmdTaxRate.Parameters.AddWithValue("@updated_date", DateTime.Now);
                cmdTaxRate.ExecuteNonQuery();

                Clear();
                MessageBox.Show("Successful");

                con.Close();
                DisplayData();
           
        }

        private void DisplayData()
        {
            con.Open();
            if (!btnTaxRateSave.Enabled)
            {
                btnTaxRateSave.Enabled = true;
            }
            DataTable dt=new DataTable();
            daTaxRate = new SqlDataAdapter(@"select id,min_taxable_income, max_taxable_income,percentage,
            (case when above=0 then 'False' else 'True' end) As Above,
            is_deleted, created_date, updated_date from Tax_Rate where is_deleted=0", con);
            daTaxRate.Fill(dt);
            dgvTaxRate.DataSource = dt;
            con.Close();
        }
        void Clear()
        {
            txtMinTaxableIncome.Text = "";
            txtMaxTaxableIncome.Text= "";
            txtPercentage.Text = "";
        }

        private void dgvTaxRate_Click(object sender, EventArgs e)
        {
            txtMinTaxableIncome.Text = dgvTaxRate.CurrentRow.Cells[1].Value.ToString();
            txtMaxTaxableIncome.Text = dgvTaxRate.CurrentRow.Cells[2].Value.ToString();
            txtPercentage.Text = dgvTaxRate.CurrentRow.Cells[3].Value.ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)
            con.Open();

            if (chkAbove.Checked == true)
            {
                above = true;
            }
            else
            {
                above = false;
                if (!decimal.TryParse(txtMaxTaxableIncome.Text, out d))
                {
                    MessageBox.Show("Please enter Maximum Taxable Income");
                    return;
                }
                else if (Decimal.Parse(txtMaxTaxableIncome.Text) < 0)
                {
                    MessageBox.Show("Please enter correct Maximum Taxable Income");
                    return;
                }
            }

         
                        
            if (!decimal.TryParse(txtMinTaxableIncome.Text, out d))
            {
                MessageBox.Show("Please enter Minimum Taxable Income");
                return;
            }
            else if (Decimal.Parse(txtMinTaxableIncome.Text) < 0)
            {
                MessageBox.Show("Please enter correct Minimum Taxable Income");
                return;
            }



            if (chkAbove.Checked == true)
            {
                above = true;
            }
            else
            {
                above = false;
                if (!decimal.TryParse(txtMaxTaxableIncome.Text, out d))
                {
                    MessageBox.Show("Please enter Maximum Taxable Income");
                    return;
                }
                else if (Decimal.Parse(txtMaxTaxableIncome.Text) < 0)
                {
                    MessageBox.Show("Please enter correct Maximum Taxable Income");
                    return;
                }
                if (Decimal.Parse(txtMaxTaxableIncome.Text) < Decimal.Parse(txtMinTaxableIncome.Text))
                {
                    MessageBox.Show("minimum is greater than maximum");
                    return;
                }
            }



           
            if (!decimal.TryParse(txtPercentage.Text, out d))
            {
                MessageBox.Show("Please enter Percentage");
                return;
            }
            else if (Decimal.Parse(txtPercentage.Text) < 0)
            {
                MessageBox.Show("Please enter correct Percentage");
                return;
            }                    
            
                int TaxRateID = int.Parse(dgvTaxRate.SelectedRows[0].Cells["col_taxrate_id"].Value.ToString());
                cmdTaxRate = new SqlCommand(@"update Tax_Rate set min_taxable_income=@min_taxable_income,
                max_taxable_income=@max_taxable_income,
                percentage=@percentage, above=@above, updated_date=@updated_date where id=@id", con);
                cmdTaxRate.Parameters.AddWithValue("@id", TaxRateID);
                cmdTaxRate.Parameters.AddWithValue("@min_taxable_income", txtMinTaxableIncome.Text);
                
           
                if (chkAbove.Checked == true)
                {
                    
                    cmdTaxRate.Parameters.AddWithValue("@max_taxable_income", DBNull.Value);

                }
                else
                {
                    cmdTaxRate.Parameters.AddWithValue("@max_taxable_income", txtMaxTaxableIncome.Text);
                }

                cmdTaxRate.Parameters.AddWithValue("@percentage", txtPercentage.Text);
                cmdTaxRate.Parameters.AddWithValue("@above", above);
                cmdTaxRate.Parameters.AddWithValue("@updated_date", DateTime.Now);
                cmdTaxRate.ExecuteNonQuery();
                MessageBox.Show("Updated Successfully");
                con.Close();
                DisplayData();
                if (!btnTaxRateSave.Enabled)
                {
                    btnTaxRateSave.Enabled = true;
                }
                if (btnUpdate.Enabled)
                {
                    btnUpdate.Enabled = false;
                }
                Clear();
            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)
            con.Open();

            DialogResult result = MessageBox.Show("Are you sure you want to delete?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                int TaxRateID = int.Parse(dgvTaxRate.SelectedRows[0].Cells["col_taxrate_id"].Value.ToString());
                cmdTaxRate = new SqlCommand("UPDATE Tax_Rate SET  is_deleted=1 where id=@id", con);
                cmdTaxRate.Parameters.AddWithValue("@id", TaxRateID);
                cmdTaxRate.ExecuteNonQuery();
              
                con.Close();
                MessageBox.Show("Deleted Successfully!");
                DisplayData();
            }

            con.Close();
        }

        private void dgvTaxRate_DoubleClick(object sender, EventArgs e)
        {
            if (!btnUpdate.Enabled)
            {
                btnUpdate.Enabled = true;
            }
            if (btnTaxRateSave.Enabled)
            {
                btnTaxRateSave.Enabled = false;
            }
            btnTaxRateSave.Enabled = false;
            txtMinTaxableIncome.Text = dgvTaxRate.CurrentRow.Cells[2].Value.ToString();
            txtMaxTaxableIncome.Text = dgvTaxRate.CurrentRow.Cells[3].Value.ToString();
            txtPercentage.Text = dgvTaxRate.CurrentRow.Cells[4].Value.ToString();
            if (this.dgvTaxRate.CurrentRow.Cells[5].Value.Equals("True"))
                chkAbove.Checked = true;
            else
                chkAbove.Checked = false;

            
        }

        private void dgvTaxRate_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dgvTaxRate.Rows[e.RowIndex].Cells["col_No"].Value = e.RowIndex + 1;
        }

        private void chkAbove_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAbove.Checked == true)
            {
                txtMaxTaxableIncome.ReadOnly = true;
                txtMaxTaxableIncome.Clear();
            }
            else
                txtMaxTaxableIncome.ReadOnly = false;
        } 
    }
}