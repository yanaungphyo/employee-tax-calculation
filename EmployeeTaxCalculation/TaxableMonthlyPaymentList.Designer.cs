﻿namespace EmployeeTaxCalculation
{
    partial class frmTaxableMonthlyPaymentList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            this.chkEmpName = new System.Windows.Forms.CheckBox();
            this.cmbEmpName = new System.Windows.Forms.ComboBox();
            this.chkMonth = new System.Windows.Forms.CheckBox();
            this.dtpFromMonth = new System.Windows.Forms.DateTimePicker();
            this.dtpToMonth = new System.Windows.Forms.DateTimePicker();
            this.dgvMonthlyList = new System.Windows.Forms.DataGridView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblTo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_empname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_month = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_monthlysalary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_annualsalary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_personalrelief = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_parentrelief = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_spouserelief = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_childrelief = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_other = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_taxableincome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_totaltax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_monthlytax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonthlyList)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkEmpName
            // 
            this.chkEmpName.AutoSize = true;
            this.chkEmpName.Location = new System.Drawing.Point(7, 16);
            this.chkEmpName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkEmpName.Name = "chkEmpName";
            this.chkEmpName.Size = new System.Drawing.Size(128, 28);
            this.chkEmpName.TabIndex = 0;
            this.chkEmpName.Text = "Employee Name";
            this.chkEmpName.UseVisualStyleBackColor = true;
            // 
            // cmbEmpName
            // 
            this.cmbEmpName.FormattingEnabled = true;
            this.cmbEmpName.Location = new System.Drawing.Point(141, 14);
            this.cmbEmpName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cmbEmpName.Name = "cmbEmpName";
            this.cmbEmpName.Size = new System.Drawing.Size(140, 30);
            this.cmbEmpName.TabIndex = 1;
            // 
            // chkMonth
            // 
            this.chkMonth.AutoSize = true;
            this.chkMonth.Location = new System.Drawing.Point(294, 17);
            this.chkMonth.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkMonth.Name = "chkMonth";
            this.chkMonth.Size = new System.Drawing.Size(104, 28);
            this.chkMonth.TabIndex = 2;
            this.chkMonth.Text = "Month From";
            this.chkMonth.UseVisualStyleBackColor = true;
            // 
            // dtpFromMonth
            // 
            this.dtpFromMonth.CustomFormat = "MMM/yyyyy";
            this.dtpFromMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromMonth.Location = new System.Drawing.Point(403, 14);
            this.dtpFromMonth.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dtpFromMonth.Name = "dtpFromMonth";
            this.dtpFromMonth.ShowUpDown = true;
            this.dtpFromMonth.Size = new System.Drawing.Size(103, 30);
            this.dtpFromMonth.TabIndex = 3;
            // 
            // dtpToMonth
            // 
            this.dtpToMonth.CustomFormat = "MMM/yyyyy";
            this.dtpToMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToMonth.Location = new System.Drawing.Point(544, 13);
            this.dtpToMonth.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dtpToMonth.Name = "dtpToMonth";
            this.dtpToMonth.ShowUpDown = true;
            this.dtpToMonth.Size = new System.Drawing.Size(103, 30);
            this.dtpToMonth.TabIndex = 4;
            // 
            // dgvMonthlyList
            // 
            this.dgvMonthlyList.AllowUserToAddRows = false;
            this.dgvMonthlyList.AllowUserToDeleteRows = false;
            this.dgvMonthlyList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMonthlyList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_id,
            this.col_no,
            this.col_empname,
            this.col_month,
            this.col_year,
            this.col_monthlysalary,
            this.col_annualsalary,
            this.col_personalrelief,
            this.col_parentrelief,
            this.col_spouserelief,
            this.col_childrelief,
            this.col_other,
            this.col_taxableincome,
            this.col_totaltax,
            this.col_monthlytax});
            this.dgvMonthlyList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMonthlyList.Location = new System.Drawing.Point(0, 0);
            this.dgvMonthlyList.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dgvMonthlyList.MultiSelect = false;
            this.dgvMonthlyList.Name = "dgvMonthlyList";
            this.dgvMonthlyList.ReadOnly = true;
            this.dgvMonthlyList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMonthlyList.Size = new System.Drawing.Size(1042, 753);
            this.dgvMonthlyList.TabIndex = 5;
            this.dgvMonthlyList.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvMonthlyList_RowPostPaint);
            // 
            // btnSearch
            // 
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.Location = new System.Drawing.Point(685, 13);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(100, 30);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Location = new System.Drawing.Point(56, 16);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 30);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(512, 20);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(26, 24);
            this.lblTo.TabIndex = 9;
            this.lblTo.Text = "To";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtpFromMonth);
            this.panel1.Controls.Add(this.lblTo);
            this.panel1.Controls.Add(this.chkEmpName);
            this.panel1.Controls.Add(this.cmbEmpName);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.chkMonth);
            this.panel1.Controls.Add(this.dtpToMonth);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1042, 54);
            this.panel1.TabIndex = 10;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 807);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1042, 58);
            this.panel2.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnDelete);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(874, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(168, 58);
            this.panel4.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvMonthlyList);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 54);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1042, 753);
            this.panel3.TabIndex = 12;
            // 
            // col_id
            // 
            this.col_id.DataPropertyName = "id";
            this.col_id.HeaderText = "id";
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            this.col_id.Visible = false;
            // 
            // col_no
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_no.DefaultCellStyle = dataGridViewCellStyle1;
            this.col_no.HeaderText = "No";
            this.col_no.Name = "col_no";
            this.col_no.ReadOnly = true;
            // 
            // col_empname
            // 
            this.col_empname.DataPropertyName = "employee_name";
            this.col_empname.HeaderText = "Employee Name";
            this.col_empname.Name = "col_empname";
            this.col_empname.ReadOnly = true;
            // 
            // col_month
            // 
            this.col_month.DataPropertyName = "taxable_month";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_month.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_month.HeaderText = "Taxable Month";
            this.col_month.Name = "col_month";
            this.col_month.ReadOnly = true;
            // 
            // col_year
            // 
            this.col_year.DataPropertyName = "taxable_year";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_year.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_year.HeaderText = "Year";
            this.col_year.Name = "col_year";
            this.col_year.ReadOnly = true;
            // 
            // col_monthlysalary
            // 
            this.col_monthlysalary.DataPropertyName = "monthly_salary";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_monthlysalary.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_monthlysalary.HeaderText = "Monthly Salary";
            this.col_monthlysalary.Name = "col_monthlysalary";
            this.col_monthlysalary.ReadOnly = true;
            // 
            // col_annualsalary
            // 
            this.col_annualsalary.DataPropertyName = "annual_salary";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_annualsalary.DefaultCellStyle = dataGridViewCellStyle5;
            this.col_annualsalary.HeaderText = "Annual Salary";
            this.col_annualsalary.Name = "col_annualsalary";
            this.col_annualsalary.ReadOnly = true;
            // 
            // col_personalrelief
            // 
            this.col_personalrelief.DataPropertyName = "personal_relief";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_personalrelief.DefaultCellStyle = dataGridViewCellStyle6;
            this.col_personalrelief.HeaderText = "Personal Relief";
            this.col_personalrelief.Name = "col_personalrelief";
            this.col_personalrelief.ReadOnly = true;
            // 
            // col_parentrelief
            // 
            this.col_parentrelief.DataPropertyName = "parents_relief";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_parentrelief.DefaultCellStyle = dataGridViewCellStyle7;
            this.col_parentrelief.HeaderText = "Parent Relief";
            this.col_parentrelief.Name = "col_parentrelief";
            this.col_parentrelief.ReadOnly = true;
            // 
            // col_spouserelief
            // 
            this.col_spouserelief.DataPropertyName = "spouse_relief";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_spouserelief.DefaultCellStyle = dataGridViewCellStyle8;
            this.col_spouserelief.HeaderText = "Spouse Relief";
            this.col_spouserelief.Name = "col_spouserelief";
            this.col_spouserelief.ReadOnly = true;
            // 
            // col_childrelief
            // 
            this.col_childrelief.DataPropertyName = "child_relief";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_childrelief.DefaultCellStyle = dataGridViewCellStyle9;
            this.col_childrelief.HeaderText = "Child Relief";
            this.col_childrelief.Name = "col_childrelief";
            this.col_childrelief.ReadOnly = true;
            // 
            // col_other
            // 
            this.col_other.DataPropertyName = "other_deductible";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_other.DefaultCellStyle = dataGridViewCellStyle10;
            this.col_other.HeaderText = "Other Deductible";
            this.col_other.Name = "col_other";
            this.col_other.ReadOnly = true;
            // 
            // col_taxableincome
            // 
            this.col_taxableincome.DataPropertyName = "taxable_income";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_taxableincome.DefaultCellStyle = dataGridViewCellStyle11;
            this.col_taxableincome.HeaderText = "Taxable Income";
            this.col_taxableincome.Name = "col_taxableincome";
            this.col_taxableincome.ReadOnly = true;
            // 
            // col_totaltax
            // 
            this.col_totaltax.DataPropertyName = "total_tax";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_totaltax.DefaultCellStyle = dataGridViewCellStyle12;
            this.col_totaltax.HeaderText = "Total Tax";
            this.col_totaltax.Name = "col_totaltax";
            this.col_totaltax.ReadOnly = true;
            // 
            // col_monthlytax
            // 
            this.col_monthlytax.DataPropertyName = "monthly_tax";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_monthlytax.DefaultCellStyle = dataGridViewCellStyle13;
            this.col_monthlytax.HeaderText = "Monthly Tax";
            this.col_monthlytax.Name = "col_monthlytax";
            this.col_monthlytax.ReadOnly = true;
            // 
            // frmTaxableMonthlyPaymentList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 865);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmTaxableMonthlyPaymentList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TaxableMonthlyPaymentList";
            this.Load += new System.EventHandler(this.frmTaxableMonthlyPaymentList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonthlyList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkEmpName;
        private System.Windows.Forms.ComboBox cmbEmpName;
        private System.Windows.Forms.CheckBox chkMonth;
        private System.Windows.Forms.DateTimePicker dtpFromMonth;
        private System.Windows.Forms.DateTimePicker dtpToMonth;
        private System.Windows.Forms.DataGridView dgvMonthlyList;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_empname;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_month;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_year;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_monthlysalary;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_annualsalary;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_personalrelief;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_parentrelief;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_spouserelief;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_childrelief;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_other;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_taxableincome;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_totaltax;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_monthlytax;
    }
}