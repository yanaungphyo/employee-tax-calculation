﻿namespace EmployeeTaxCalculation
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.btnEmpList = new System.Windows.Forms.Button();
            this.btnReliefType = new System.Windows.Forms.Button();
            this.btnTaxRate = new System.Windows.Forms.Button();
            this.btnMonthlyTax = new System.Windows.Forms.Button();
            this.btnMonthlyTaxList = new System.Windows.Forms.Button();
            this.btnYearlyTax = new System.Windows.Forms.Button();
            this.btnDetailReport = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnEmpBankReport = new System.Windows.Forms.Button();
            this.btnCurrency = new System.Windows.Forms.Button();
            this.btnEducation = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnYearlyReport = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEmpList
            // 
            this.btnEmpList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEmpList.Location = new System.Drawing.Point(0, 131);
            this.btnEmpList.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnEmpList.Name = "btnEmpList";
            this.btnEmpList.Size = new System.Drawing.Size(150, 50);
            this.btnEmpList.TabIndex = 0;
            this.btnEmpList.Text = "Employee List";
            this.btnEmpList.UseVisualStyleBackColor = true;
            this.btnEmpList.Click += new System.EventHandler(this.btnEmpList_Click);
            // 
            // btnReliefType
            // 
            this.btnReliefType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReliefType.Location = new System.Drawing.Point(0, 179);
            this.btnReliefType.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnReliefType.Name = "btnReliefType";
            this.btnReliefType.Size = new System.Drawing.Size(150, 50);
            this.btnReliefType.TabIndex = 4;
            this.btnReliefType.Text = "Relief Type";
            this.btnReliefType.UseVisualStyleBackColor = true;
            this.btnReliefType.Click += new System.EventHandler(this.btnReliefType_Click);
            // 
            // btnTaxRate
            // 
            this.btnTaxRate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTaxRate.Location = new System.Drawing.Point(0, 227);
            this.btnTaxRate.Name = "btnTaxRate";
            this.btnTaxRate.Size = new System.Drawing.Size(150, 50);
            this.btnTaxRate.TabIndex = 6;
            this.btnTaxRate.Text = "Tax Rate";
            this.btnTaxRate.UseVisualStyleBackColor = true;
            this.btnTaxRate.Click += new System.EventHandler(this.btnTaxRate_Click);
            // 
            // btnMonthlyTax
            // 
            this.btnMonthlyTax.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMonthlyTax.Location = new System.Drawing.Point(0, 275);
            this.btnMonthlyTax.Name = "btnMonthlyTax";
            this.btnMonthlyTax.Size = new System.Drawing.Size(150, 50);
            this.btnMonthlyTax.TabIndex = 7;
            this.btnMonthlyTax.Text = "Monthly Tax";
            this.btnMonthlyTax.UseVisualStyleBackColor = true;
            this.btnMonthlyTax.Click += new System.EventHandler(this.btnMonthlyTax_Click);
            // 
            // btnMonthlyTaxList
            // 
            this.btnMonthlyTaxList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMonthlyTaxList.Location = new System.Drawing.Point(0, 370);
            this.btnMonthlyTaxList.Name = "btnMonthlyTaxList";
            this.btnMonthlyTaxList.Size = new System.Drawing.Size(150, 50);
            this.btnMonthlyTaxList.TabIndex = 8;
            this.btnMonthlyTaxList.Text = "Monthly Tax List";
            this.btnMonthlyTaxList.UseVisualStyleBackColor = true;
            this.btnMonthlyTaxList.Click += new System.EventHandler(this.btnMonthlyTaxList_Click);
            // 
            // btnYearlyTax
            // 
            this.btnYearlyTax.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnYearlyTax.Location = new System.Drawing.Point(0, 322);
            this.btnYearlyTax.Name = "btnYearlyTax";
            this.btnYearlyTax.Size = new System.Drawing.Size(150, 50);
            this.btnYearlyTax.TabIndex = 9;
            this.btnYearlyTax.Text = "Yearly Tax";
            this.btnYearlyTax.UseVisualStyleBackColor = true;
            this.btnYearlyTax.Click += new System.EventHandler(this.btnYearlyTax_Click);
            // 
            // btnDetailReport
            // 
            this.btnDetailReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDetailReport.Location = new System.Drawing.Point(0, 417);
            this.btnDetailReport.Name = "btnDetailReport";
            this.btnDetailReport.Size = new System.Drawing.Size(150, 50);
            this.btnDetailReport.TabIndex = 10;
            this.btnDetailReport.Text = "Tax Detail Report";
            this.btnDetailReport.UseVisualStyleBackColor = true;
            this.btnDetailReport.Click += new System.EventHandler(this.btnDetailReport_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnEmpBankReport);
            this.panel1.Controls.Add(this.btnCurrency);
            this.panel1.Controls.Add(this.btnEducation);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.btnYearlyReport);
            this.panel1.Controls.Add(this.btnEmpList);
            this.panel1.Controls.Add(this.btnReliefType);
            this.panel1.Controls.Add(this.btnTaxRate);
            this.panel1.Controls.Add(this.btnYearlyTax);
            this.panel1.Controls.Add(this.btnDetailReport);
            this.panel1.Controls.Add(this.btnMonthlyTaxList);
            this.panel1.Controls.Add(this.btnMonthlyTax);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(150, 692);
            this.panel1.TabIndex = 11;
            // 
            // btnEmpBankReport
            // 
            this.btnEmpBankReport.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.btnEmpBankReport.Location = new System.Drawing.Point(0, 612);
            this.btnEmpBankReport.Name = "btnEmpBankReport";
            this.btnEmpBankReport.Size = new System.Drawing.Size(150, 57);
            this.btnEmpBankReport.TabIndex = 15;
            this.btnEmpBankReport.Text = "Employee Bank Account History Report";
            this.btnEmpBankReport.UseVisualStyleBackColor = true;
            this.btnEmpBankReport.Click += new System.EventHandler(this.btnEmpBankReport_Click);
            // 
            // btnCurrency
            // 
            this.btnCurrency.Location = new System.Drawing.Point(0, 562);
            this.btnCurrency.Name = "btnCurrency";
            this.btnCurrency.Size = new System.Drawing.Size(150, 50);
            this.btnCurrency.TabIndex = 0;
            this.btnCurrency.Text = "Currency";
            this.btnCurrency.UseVisualStyleBackColor = true;
            this.btnCurrency.Click += new System.EventHandler(this.btnCurrency_Click);
            // 
            // btnEducation
            // 
            this.btnEducation.Location = new System.Drawing.Point(0, 514);
            this.btnEducation.Name = "btnEducation";
            this.btnEducation.Size = new System.Drawing.Size(150, 50);
            this.btnEducation.TabIndex = 14;
            this.btnEducation.Text = "Education";
            this.btnEducation.UseVisualStyleBackColor = true;
            this.btnEducation.Click += new System.EventHandler(this.btnEducation_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(150, 132);
            this.panel3.TabIndex = 13;
            // 
            // btnYearlyReport
            // 
            this.btnYearlyReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnYearlyReport.Location = new System.Drawing.Point(0, 465);
            this.btnYearlyReport.Name = "btnYearlyReport";
            this.btnYearlyReport.Size = new System.Drawing.Size(150, 50);
            this.btnYearlyReport.TabIndex = 12;
            this.btnYearlyReport.Text = "Tax Yearly Report";
            this.btnYearlyReport.UseVisualStyleBackColor = true;
            this.btnYearlyReport.Click += new System.EventHandler(this.btnYearlyReport_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panel2.Location = new System.Drawing.Point(150, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(712, 692);
            this.panel2.TabIndex = 12;
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(862, 692);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEmpList;
        private System.Windows.Forms.Button btnReliefType;
        private System.Windows.Forms.Button btnTaxRate;
        private System.Windows.Forms.Button btnMonthlyTax;
        private System.Windows.Forms.Button btnMonthlyTaxList;
        private System.Windows.Forms.Button btnYearlyTax;
        private System.Windows.Forms.Button btnDetailReport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnYearlyReport;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnEducation;
        private System.Windows.Forms.Button btnCurrency;
        private System.Windows.Forms.Button btnEmpBankReport;
    }
}