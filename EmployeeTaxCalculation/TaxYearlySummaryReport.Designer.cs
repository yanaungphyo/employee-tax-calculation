﻿namespace EmployeeTaxCalculation
{
    partial class frmTaxYearlySummaryReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.Tax_Yearly_ReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.EmployeeTaxCalculationDataSet = new EmployeeTaxCalculation.EmployeeTaxCalculationDataSet();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblTo = new System.Windows.Forms.Label();
            this.dtpToYear = new System.Windows.Forms.DateTimePicker();
            this.dtpFromYear = new System.Windows.Forms.DateTimePicker();
            this.cmbEmpName = new System.Windows.Forms.ComboBox();
            this.chkYear = new System.Windows.Forms.CheckBox();
            this.chkEmpName = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rpvTaxYearly = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.Tax_Yearly_ReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeTaxCalculationDataSet)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tax_Yearly_ReportBindingSource
            // 
            this.Tax_Yearly_ReportBindingSource.DataMember = "Tax_Yearly_Report";
            this.Tax_Yearly_ReportBindingSource.DataSource = this.EmployeeTaxCalculationDataSet;
            // 
            // EmployeeTaxCalculationDataSet
            // 
            this.EmployeeTaxCalculationDataSet.DataSetName = "EmployeeTaxCalculationDataSet";
            this.EmployeeTaxCalculationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.lblTo);
            this.panel1.Controls.Add(this.dtpToYear);
            this.panel1.Controls.Add(this.dtpFromYear);
            this.panel1.Controls.Add(this.cmbEmpName);
            this.panel1.Controls.Add(this.chkYear);
            this.panel1.Controls.Add(this.chkEmpName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(816, 94);
            this.panel1.TabIndex = 0;
            // 
            // btnSearch
            // 
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.Location = new System.Drawing.Point(316, 53);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(100, 30);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(193, 57);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(26, 24);
            this.lblTo.TabIndex = 5;
            this.lblTo.Text = "To";
            // 
            // dtpToYear
            // 
            this.dtpToYear.CustomFormat = "yyyy";
            this.dtpToYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToYear.Location = new System.Drawing.Point(225, 53);
            this.dtpToYear.Name = "dtpToYear";
            this.dtpToYear.ShowUpDown = true;
            this.dtpToYear.Size = new System.Drawing.Size(76, 30);
            this.dtpToYear.TabIndex = 4;
            // 
            // dtpFromYear
            // 
            this.dtpFromYear.CustomFormat = "yyyy";
            this.dtpFromYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromYear.Location = new System.Drawing.Point(110, 53);
            this.dtpFromYear.Name = "dtpFromYear";
            this.dtpFromYear.ShowUpDown = true;
            this.dtpFromYear.Size = new System.Drawing.Size(77, 30);
            this.dtpFromYear.TabIndex = 3;
            // 
            // cmbEmpName
            // 
            this.cmbEmpName.FormattingEnabled = true;
            this.cmbEmpName.Location = new System.Drawing.Point(147, 13);
            this.cmbEmpName.Name = "cmbEmpName";
            this.cmbEmpName.Size = new System.Drawing.Size(121, 30);
            this.cmbEmpName.TabIndex = 2;
            // 
            // chkYear
            // 
            this.chkYear.AutoSize = true;
            this.chkYear.Location = new System.Drawing.Point(13, 57);
            this.chkYear.Name = "chkYear";
            this.chkYear.Size = new System.Drawing.Size(91, 28);
            this.chkYear.TabIndex = 1;
            this.chkYear.Text = "Year From";
            this.chkYear.UseVisualStyleBackColor = true;
            // 
            // chkEmpName
            // 
            this.chkEmpName.AutoSize = true;
            this.chkEmpName.Location = new System.Drawing.Point(13, 13);
            this.chkEmpName.Name = "chkEmpName";
            this.chkEmpName.Size = new System.Drawing.Size(128, 28);
            this.chkEmpName.TabIndex = 0;
            this.chkEmpName.Text = "Employee Name";
            this.chkEmpName.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rpvTaxYearly);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 94);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(816, 378);
            this.panel2.TabIndex = 1;
            // 
            // rpvTaxYearly
            // 
            this.rpvTaxYearly.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource2.Name = "Tax_Yearly_Report";
            reportDataSource2.Value = this.Tax_Yearly_ReportBindingSource;
            this.rpvTaxYearly.LocalReport.DataSources.Add(reportDataSource2);
            this.rpvTaxYearly.LocalReport.ReportEmbeddedResource = "EmployeeTaxCalculation.TaxYearlySummaryReport.rdlc";
            this.rpvTaxYearly.Location = new System.Drawing.Point(0, 0);
            this.rpvTaxYearly.Name = "rpvTaxYearly";
            this.rpvTaxYearly.Size = new System.Drawing.Size(816, 378);
            this.rpvTaxYearly.TabIndex = 0;
            // 
            // frmTaxYearlySummaryReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 472);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmTaxYearlySummaryReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TaxYearlySummaryReport";
            this.Load += new System.EventHandler(this.TaxYearlySummaryReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Tax_Yearly_ReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeTaxCalculationDataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.DateTimePicker dtpToYear;
        private System.Windows.Forms.DateTimePicker dtpFromYear;
        private System.Windows.Forms.ComboBox cmbEmpName;
        private System.Windows.Forms.CheckBox chkYear;
        private System.Windows.Forms.CheckBox chkEmpName;
        private Microsoft.Reporting.WinForms.ReportViewer rpvTaxYearly;
        private System.Windows.Forms.BindingSource Tax_Yearly_ReportBindingSource;
        private EmployeeTaxCalculationDataSet EmployeeTaxCalculationDataSet;
    }
}