﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmTaxableMonthlyPayment : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmdMonthlyTax;
        SqlDataAdapter daMonthlyTax;
        DataTable dt;
        public frmTaxableMonthlyPayment()
        {
            InitializeComponent();
            dgvMonthly.AutoGenerateColumns = false;

            this.bgwMonthlyCalculate = new BackgroundWorker();
            this.bgwMonthlyCalculate.DoWork += new DoWorkEventHandler(bgwMonthlyCalculate_DoWork);
            this.bgwMonthlyCalculate.ProgressChanged += new ProgressChangedEventHandler(bgwMonthlyCalculate_ProgressChanged);
            this.bgwMonthlyCalculate.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgwMonthlyCalculate_RunWorkerCompleted);
            this.bgwMonthlyCalculate.WorkerReportsProgress = true;

            this.btnCalculate.Click += new EventHandler(btnCalculate_Click);
            this.dgvMonthly.Columns["col_personal_relief"].DefaultCellStyle.Format = "0.00##";
            this.dgvMonthly.Columns["col_0_20Lakh"].DefaultCellStyle.Format = "0.00##";
            this.dgvMonthly.Columns["col_20Lakh_50Lakh"].DefaultCellStyle.Format = "0.00##";
            this.dgvMonthly.Columns["col_50Lakh_100Lakh"].DefaultCellStyle.Format = "0.00##";
            this.dgvMonthly.Columns["col_100Lakh_200Lakh"].DefaultCellStyle.Format = "0.00##";
            this.dgvMonthly.Columns["col_200Lakh_300Lakh"].DefaultCellStyle.Format = "0.00##";
            this.dgvMonthly.Columns["col_300Lakh_above"].DefaultCellStyle.Format = "0.00##";

            this.dgvMonthly.Columns["col_total_tax"].DefaultCellStyle.Format = "0.00##";
            this.dgvMonthly.Columns["col_monthly_tax"].DefaultCellStyle.Format = "0.##";
          
        }
        
        private void frmTaxableMonthlyPayment_Load(object sender, EventArgs e)
        {
            con.Open();
            dtpMonth.Value = new DateTime(2019,1,1);
            
            string querySelect = "select id,employee_name from Employee where is_deleted=0";
            cmdMonthlyTax = new SqlCommand(querySelect, con);
            daMonthlyTax = new SqlDataAdapter(cmdMonthlyTax);
            dt = new DataTable();
            daMonthlyTax.Fill(dt);

            cmbEmployee.DisplayMember = "employee_name";
            cmbEmployee.ValueMember = "id";
            cmbEmployee.DataSource = dt;
            this.cmbEmployee.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cmbEmployee.AutoCompleteSource = AutoCompleteSource.ListItems;
            con.Close();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)
            con.Open();
            


                cmdMonthlyTax = new SqlCommand("exec Tax_Calculation_Stp", con);
                daMonthlyTax = new SqlDataAdapter(cmdMonthlyTax);
                con.Close();
                dt = new DataTable();
                daMonthlyTax.Fill(dt);

                if (chkEmployee.Checked)
                {
                    if (cmbEmployee.SelectedValue == null)
                    {
                        MessageBox.Show("Please choose Employee Name!");
                        cmbEmployee.Focus();
                        return;
                    }
                    else
                    {
                        string expression = "EmpID = " + cmbEmployee.SelectedValue.ToString();
                        DataRow[] rows = dt.Select(expression);
                        dt = rows.CopyToDataTable();
                    }
                }

                dgvMonthly.DataSource = dt;

                if (!bgwMonthlyCalculate.IsBusy)
                {
                    pbMonthlyCalculate.Value = 0;
                    pbMonthlyCalculate.Maximum = dgvMonthly.Rows.Count;
                    bgwMonthlyCalculate.RunWorkerAsync();
                    btnCalculate.Enabled = false;
                }
          
        }
               
        private void dgvMonthly_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dgvMonthly.Rows[e.RowIndex].Cells["col_no"].Value = e.RowIndex + 1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {          
            
            for (int i = 0; i < dgvMonthly.RowCount; i++)
            {
                int empID = int.Parse(dgvMonthly.Rows[i].Cells["col_empid"].Value.ToString());

                if (con.State != ConnectionState.Open)
                    con.Open();
                string validationQuery = "select * From dbo.Taxable_Monthly_Payment where is_deleted = 0 and employee_id = " + empID + " and taxable_month = " + dtpMonth.Value.Month + " and taxable_year = " + dtpMonth.Value.Year;
                cmdMonthlyTax = new SqlCommand(validationQuery, con);
                daMonthlyTax = new SqlDataAdapter(cmdMonthlyTax);
                con.Close();
                DataTable Validationdt = new DataTable();
                daMonthlyTax.Fill(Validationdt);
                if (Validationdt.Rows.Count != 0)
                {
                    MessageBox.Show(dgvMonthly.Rows[i].Cells["col_empname"].Value.ToString() + " is already calculated for " + dtpMonth.Value.Month + "/" + dtpMonth.Value.Year);
                }
                else
                {
                    if (con.State != ConnectionState.Open)
                        con.Open();
                    string query = @"INSERT INTO Taxable_Monthly_Payment
                                (employee_id, taxable_month, taxable_year, monthly_salary, annual_salary, personal_relief, parents_relief, spouse_relief, child_relief, taxable_income, total_tax, monthly_tax, is_deleted, created_date, updated_date) 
                                Values
                                (@employee_id, @taxable_month, @taxable_year, @monthly_salary, @annual_salary, @personal_relief, @parents_relief, @spouse_relief, @child_relief, @taxable_income, @total_tax, @monthly_tax, @is_deleted, @created_date, @updated_date) ";
                    cmdMonthlyTax = new SqlCommand(query, con);

                    cmdMonthlyTax.Parameters.AddWithValue("@employee_id", empID);
                    cmdMonthlyTax.Parameters.AddWithValue("@taxable_month", dtpMonth.Value.Month);
                    cmdMonthlyTax.Parameters.AddWithValue("@taxable_year", dtpMonth.Value.Year);
                    cmdMonthlyTax.Parameters.AddWithValue("@monthly_salary", dgvMonthly.Rows[i].Cells["col_monthly_salary"].Value.ToString());
                    cmdMonthlyTax.Parameters.AddWithValue("@annual_salary", dgvMonthly.Rows[i].Cells["col_annual_salary"].Value.ToString());
                    cmdMonthlyTax.Parameters.AddWithValue("@personal_relief", dgvMonthly.Rows[i].Cells["col_personal_relief"].Value.ToString());
                    cmdMonthlyTax.Parameters.AddWithValue("@parents_relief", dgvMonthly.Rows[i].Cells["col_parents_relief"].Value.ToString());
                    cmdMonthlyTax.Parameters.AddWithValue("@spouse_relief", dgvMonthly.Rows[i].Cells["col_spouse_relief"].Value.ToString());
                    cmdMonthlyTax.Parameters.AddWithValue("@child_relief", dgvMonthly.Rows[i].Cells["col_child_relief"].Value.ToString());
                    cmdMonthlyTax.Parameters.AddWithValue("@other_deductible", dgvMonthly.Rows[i].Cells["col_other"].Value != null ? dgvMonthly.Rows[i].Cells["col_other"].Value.ToString() : "0");
                    cmdMonthlyTax.Parameters.AddWithValue("@taxable_income", dgvMonthly.Rows[i].Cells["col_taxable_income"].Value.ToString());
                    cmdMonthlyTax.Parameters.AddWithValue("@total_tax", dgvMonthly.Rows[i].Cells["col_total_tax"].Value.ToString());
                    cmdMonthlyTax.Parameters.AddWithValue("@monthly_tax", dgvMonthly.Rows[i].Cells["col_monthly_tax"].Value.ToString());
                    cmdMonthlyTax.Parameters.AddWithValue("@is_deleted", Boolean.FalseString);
                    cmdMonthlyTax.Parameters.AddWithValue("@created_date", DateTime.Now);
                    cmdMonthlyTax.Parameters.AddWithValue("@updated_date", DateTime.Now);
                    cmdMonthlyTax.ExecuteNonQuery();
                }


            }

            MessageBox.Show("Success");
            dgvMonthly.DataSource = null;
             
            con.Close();
        }

        private void bgwMonthlyCalculate_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            for (int i = 1; i <= dgvMonthly.RowCount; i++)
            {
                worker.ReportProgress(0);
                Thread.Sleep(100);
            
            }
        }

        private void bgwMonthlyCalculate_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbMonthlyCalculate.Value =+dgvMonthly.Rows.Count;
        }

        private void bgwMonthlyCalculate_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("Successful");
            btnCalculate.Enabled = true;
        }

       
       
        
        
        
    }
}

/*
cmdMonthlyTax = new SqlCommand("insert into Taxable_Monthly_Payment (employee_id,taxable_month,taxable_year, monthly_salary, annual_salary, personal_relief, parents_relief, spouse_relief, child_relief, other_deductible, taxable_income, total_tax, monthly_tax ,is_deleted,created_date,updated_date) values (@employee_id,@taxable_month,@taxable_year, @monthly_salary, @annual_salary, @personal_relief, @parents_relief, @spouse_relief, @child_relief, @other_deductible, @taxable_income, @total_tax, @monthly_tax ,@is_deleted, @created_date,@updated_date)", con);
cmdMonthlyTax.Parameters.AddWithValue("@employee_id", cmbEmployee.SelectedValue);
cmdMonthlyTax.Parameters.AddWithValue("@taxable_month", txtRefAmount.Text);
cmdMonthlyTax.Parameters.AddWithValue("@taxable_year", Boolean.FalseString);
cmdMonthlyTax.Parameters.AddWithValue("@monthly_salary", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@annual_salary", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@personal_relief", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@parents_relief", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@spouse_relief", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@child_relief", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@other_deductible", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@taxable_income", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@total_tax", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@monthly_tax", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@is_deleted", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@created_date", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@updated_date", DateTime.Now);
cmdMonthlyTax.Parameters.AddWithValue("@total_tax", DateTime.Now);
cmdMonthlyTax.ExecuteNonQuery();
*/