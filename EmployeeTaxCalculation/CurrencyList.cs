﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace EmployeeTaxCalculation
{
    public partial class frmCurrencyList : Form
    {        
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlDataAdapter da;
        SqlCommand cmd;
        public frmCurrencyList()
        {
            InitializeComponent();
            dgvCurrency.AutoGenerateColumns = false;
            DisplayData();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            con.Open();
            frmCurrencyRegistration frmCuryReg = new frmCurrencyRegistration();
            frmCuryReg.ShowDialog();
            con.Close();
            DisplayData();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                int curyId = int.Parse(dgvCurrency.SelectedRows[0].Cells["col_id"].Value.ToString());
                bool dDefault = bool.Parse(dgvCurrency.SelectedRows[0].Cells["col_default"].Value.ToString());
                bool dActive = bool.Parse(dgvCurrency.SelectedRows[0].Cells["col_active"].Value.ToString());
                frmCurrencyRegistration frmCuryReg = new frmCurrencyRegistration(curyId,
                dgvCurrency.SelectedRows[0].Cells["col_currency_symbol"].Value.ToString(),
                dgvCurrency.SelectedRows[0].Cells["col_currency_name"].Value.ToString(),
                dgvCurrency.SelectedRows[0].Cells["col_description"].Value.ToString(),
                dDefault,
                dActive);
                frmCuryReg.ShowDialog();
                con.Close();
                DisplayData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please select data");
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
                con.Open();
            try
            {
                DialogResult result = MessageBox.Show("Are you sure you want to delete?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    int curyID = int.Parse(dgvCurrency.SelectedRows[0].Cells["col_id"].Value.ToString());
                    cmd = new SqlCommand("UPDATE Currency SET  is_deleted=1 where id=@id", con);
                    cmd.Parameters.AddWithValue("@id", curyID);
                    cmd.ExecuteNonQuery();

                    con.Close();
                    MessageBox.Show("Deleted Successfully!");
                    DisplayData();
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please select data");
            }
        }
        private void DisplayData()
        {
            if (con.State != ConnectionState.Open)
                con.Open();
            string query = @"select id,currency_name,currency_symbol,description,
            (case when check_default=0 then 'false' else 'true' end) as check_default,
            (case when check_active=0 then 'false' else 'true' end) as check_active 
            from Currency where is_deleted=0;";
            da = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dgvCurrency.DataSource = dt;
            dgvCurrency.FirstDisplayedScrollingColumnIndex = dgvCurrency.ColumnCount - 1;
            con.Close();
        }

        private void dgvCurrency_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dgvCurrency.Rows[e.RowIndex].Cells["col_no"].Value = e.RowIndex + 1;
        }            

        
    }
        
}
