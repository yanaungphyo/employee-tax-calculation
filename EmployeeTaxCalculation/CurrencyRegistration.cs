﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace EmployeeTaxCalculation
{
    public partial class frmCurrencyRegistration : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        bool bdefault;
        bool bactive;
        int CuryId;
               
        
        public frmCurrencyRegistration()
        {
            InitializeComponent();
        }

        

        public frmCurrencyRegistration(int curyId, string currencySymbol, string currencyName,string description, bool bDefault, bool bActive)
        {
            InitializeComponent();
            this.CuryId = curyId;
            txtCurrencyName.Text = currencyName;
            txtCurrencySymbol.Text = currencySymbol;
            txtDescription.Text = description;
            chkDefault.Checked = bDefault;
            chkActive.Checked = bActive;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)
            con.Open();          
            

            string Query = "Select * from Currency where is_deleted=0 and currency_symbol = '" + txtCurrencySymbol.Text+"'";
            da=new SqlDataAdapter(Query,con);


                DataTable Validationdt = new DataTable();
                da.Fill(Validationdt);
                if (Validationdt.Rows.Count > 0)
                {
                    MessageBox.Show(txtCurrencySymbol.Text+" is already added");
                }
    
                else
                {
                    if (con.State != ConnectionState.Open)
                        con.Open();
                    if (CuryId == 0)
                    {
                        if (chkDefault.Checked == true)
                        { bdefault = true; }
                        else bdefault = false;

                        if (chkActive.Checked == true)
                        { bactive = true; }
                        else bactive = false;


                        string query = @"insert into Currency(currency_symbol, currency_name, description, check_default, check_active, is_deleted, created_date, updated_date)
                            values (@currency_symbol, @currency_name, @description, @check_default, @check_active, @is_deleted, @created_date, @updated_date)";
                        cmd = new SqlCommand(query, con);
                        cmd.Parameters.AddWithValue("@currency_symbol", txtCurrencySymbol.Text);
                        cmd.Parameters.AddWithValue("@currency_name", txtCurrencyName.Text);
                        cmd.Parameters.AddWithValue("@description", txtDescription.Text);
                        cmd.Parameters.AddWithValue("@check_default", bdefault);
                        cmd.Parameters.AddWithValue("@check_active", bactive);
                        cmd.Parameters.AddWithValue("@is_deleted", Boolean.FalseString);
                        cmd.Parameters.AddWithValue("@created_date", DateTime.Now);
                        cmd.Parameters.AddWithValue("@updated_date", DateTime.Now);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Success");

                    }
                    else
                    {
                        if (txtCurrencySymbol.Text == "" || txtCurrencyName.Text == "" || txtDescription.Text == "")
                        {
                            MessageBox.Show("Please fill data");
                            return;
                        }
                        if (chkDefault.Checked == true)
                        { bdefault = true; }
                        else bdefault = false;

                        if (chkActive.Checked == true)
                        { bactive = true; }
                        else bactive = false;

                        cmd = new SqlCommand(@"update Currency set currency_symbol=@currency_symbol, currency_name=@currency_name, description=@description, check_default=@check_default, check_active=@check_active, updated_date=@updated_date where id=@id", con);
                        cmd.Parameters.AddWithValue("@id", this.CuryId);
                        cmd.Parameters.AddWithValue("@currency_symbol", txtCurrencySymbol.Text);
                        cmd.Parameters.AddWithValue("@currency_name", txtCurrencyName.Text);
                        cmd.Parameters.AddWithValue("@description", txtDescription.Text);
                        cmd.Parameters.AddWithValue("@check_default", bdefault);
                        cmd.Parameters.AddWithValue("@check_active", bactive);
                        cmd.Parameters.AddWithValue("@updated_date", DateTime.Now);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Updated Successfully");

                        con.Close();
                       
                    }
                
                
                Clear();
                this.Close();
                

                con.Close();
            }
        }
        private void Clear()
        {
            txtCurrencyName.Clear();
            txtCurrencySymbol.Clear();
            txtDescription.Clear();
            chkDefault.Checked = false;
            chkDefault.Checked = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
                con.Open();
            this.Close();
            con.Close();

        }
    }
}
