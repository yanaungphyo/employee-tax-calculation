﻿namespace EmployeeTaxCalculation
{
    partial class frmEmployeeList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelEmployeeList = new System.Windows.Forms.Panel();
            this.btnEmployeeListDelete = new System.Windows.Forms.Button();
            this.btnEmployeeListUpdate = new System.Windows.Forms.Button();
            this.btnEmpoloyeeListRegister = new System.Windows.Forms.Button();
            this.dgvEmpList = new System.Windows.Forms.DataGridView();
            this.col_EmpID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_EmpName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_NRC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_monthly_net_salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_no_of_parents = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_no_of_spouse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_no_of_children = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelEmployeeList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpList)).BeginInit();
            this.SuspendLayout();
            // 
            // panelEmployeeList
            // 
            this.panelEmployeeList.Controls.Add(this.btnEmployeeListDelete);
            this.panelEmployeeList.Controls.Add(this.btnEmployeeListUpdate);
            this.panelEmployeeList.Controls.Add(this.btnEmpoloyeeListRegister);
            this.panelEmployeeList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelEmployeeList.Location = new System.Drawing.Point(0, 449);
            this.panelEmployeeList.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panelEmployeeList.Name = "panelEmployeeList";
            this.panelEmployeeList.Size = new System.Drawing.Size(764, 59);
            this.panelEmployeeList.TabIndex = 0;
            // 
            // btnEmployeeListDelete
            // 
            this.btnEmployeeListDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEmployeeListDelete.Location = new System.Drawing.Point(224, 10);
            this.btnEmployeeListDelete.Name = "btnEmployeeListDelete";
            this.btnEmployeeListDelete.Size = new System.Drawing.Size(100, 40);
            this.btnEmployeeListDelete.TabIndex = 3;
            this.btnEmployeeListDelete.Text = "Delete";
            this.btnEmployeeListDelete.UseVisualStyleBackColor = true;
            this.btnEmployeeListDelete.Click += new System.EventHandler(this.btnEmployeeListDelete_Click);
            // 
            // btnEmployeeListUpdate
            // 
            this.btnEmployeeListUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEmployeeListUpdate.Location = new System.Drawing.Point(118, 10);
            this.btnEmployeeListUpdate.Name = "btnEmployeeListUpdate";
            this.btnEmployeeListUpdate.Size = new System.Drawing.Size(100, 40);
            this.btnEmployeeListUpdate.TabIndex = 2;
            this.btnEmployeeListUpdate.Text = "Update";
            this.btnEmployeeListUpdate.UseVisualStyleBackColor = true;
            this.btnEmployeeListUpdate.Click += new System.EventHandler(this.btnEmployeeListUpdate_Click);
            // 
            // btnEmpoloyeeListRegister
            // 
            this.btnEmpoloyeeListRegister.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEmpoloyeeListRegister.Location = new System.Drawing.Point(12, 10);
            this.btnEmpoloyeeListRegister.Name = "btnEmpoloyeeListRegister";
            this.btnEmpoloyeeListRegister.Size = new System.Drawing.Size(100, 40);
            this.btnEmpoloyeeListRegister.TabIndex = 1;
            this.btnEmpoloyeeListRegister.Text = "Register";
            this.btnEmpoloyeeListRegister.UseVisualStyleBackColor = true;
            this.btnEmpoloyeeListRegister.Click += new System.EventHandler(this.btnEmpoloyeeListRegister_Click);
            // 
            // dgvEmpList
            // 
            this.dgvEmpList.AllowUserToAddRows = false;
            this.dgvEmpList.AllowUserToDeleteRows = false;
            this.dgvEmpList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmpList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_EmpID,
            this.col_No,
            this.col_EmpName,
            this.col_NRC,
            this.col_monthly_net_salary,
            this.col_no_of_parents,
            this.col_no_of_spouse,
            this.col_no_of_children});
            this.dgvEmpList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEmpList.Location = new System.Drawing.Point(0, 0);
            this.dgvEmpList.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dgvEmpList.MultiSelect = false;
            this.dgvEmpList.Name = "dgvEmpList";
            this.dgvEmpList.ReadOnly = true;
            this.dgvEmpList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmpList.Size = new System.Drawing.Size(764, 449);
            this.dgvEmpList.TabIndex = 1;
            this.dgvEmpList.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvEmpList_RowPostPaint);
            // 
            // col_EmpID
            // 
            this.col_EmpID.DataPropertyName = "id";
            this.col_EmpID.HeaderText = "EmpID";
            this.col_EmpID.Name = "col_EmpID";
            this.col_EmpID.ReadOnly = true;
            this.col_EmpID.Visible = false;
            // 
            // col_No
            // 
            this.col_No.DataPropertyName = "No";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_No.DefaultCellStyle = dataGridViewCellStyle1;
            this.col_No.HeaderText = "No";
            this.col_No.Name = "col_No";
            this.col_No.ReadOnly = true;
            this.col_No.Width = 70;
            // 
            // col_EmpName
            // 
            this.col_EmpName.DataPropertyName = "employee_name";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.col_EmpName.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_EmpName.HeaderText = "Employee Name";
            this.col_EmpName.Name = "col_EmpName";
            this.col_EmpName.ReadOnly = true;
            this.col_EmpName.Width = 200;
            // 
            // col_NRC
            // 
            this.col_NRC.DataPropertyName = "NRC_No";
            this.col_NRC.HeaderText = "NRC No.";
            this.col_NRC.Name = "col_NRC";
            this.col_NRC.ReadOnly = true;
            this.col_NRC.Width = 200;
            // 
            // col_monthly_net_salary
            // 
            this.col_monthly_net_salary.DataPropertyName = "monthly_net_salary";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_monthly_net_salary.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_monthly_net_salary.HeaderText = "Monthly Net Salary";
            this.col_monthly_net_salary.Name = "col_monthly_net_salary";
            this.col_monthly_net_salary.ReadOnly = true;
            this.col_monthly_net_salary.Width = 150;
            // 
            // col_no_of_parents
            // 
            this.col_no_of_parents.DataPropertyName = "no_of_parents";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_no_of_parents.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_no_of_parents.HeaderText = "Number of Parents";
            this.col_no_of_parents.Name = "col_no_of_parents";
            this.col_no_of_parents.ReadOnly = true;
            // 
            // col_no_of_spouse
            // 
            this.col_no_of_spouse.DataPropertyName = "no_of_spouse";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_no_of_spouse.DefaultCellStyle = dataGridViewCellStyle5;
            this.col_no_of_spouse.HeaderText = "Number of Spouse";
            this.col_no_of_spouse.Name = "col_no_of_spouse";
            this.col_no_of_spouse.ReadOnly = true;
            // 
            // col_no_of_children
            // 
            this.col_no_of_children.DataPropertyName = "no_of_children";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_no_of_children.DefaultCellStyle = dataGridViewCellStyle6;
            this.col_no_of_children.HeaderText = "Number of Children";
            this.col_no_of_children.Name = "col_no_of_children";
            this.col_no_of_children.ReadOnly = true;
            // 
            // frmEmployeeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 508);
            this.Controls.Add(this.dgvEmpList);
            this.Controls.Add(this.panelEmployeeList);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmEmployeeList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmployeeList";
            this.panelEmployeeList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelEmployeeList;
        private System.Windows.Forms.DataGridView dgvEmpList;
        private System.Windows.Forms.Button btnEmployeeListDelete;
        private System.Windows.Forms.Button btnEmployeeListUpdate;
        private System.Windows.Forms.Button btnEmpoloyeeListRegister;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_EmpID;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_No;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_EmpName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_NRC;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_monthly_net_salary;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_no_of_parents;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_no_of_spouse;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_no_of_children;
    }
}