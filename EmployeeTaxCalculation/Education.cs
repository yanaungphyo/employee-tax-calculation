﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmEducation : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlDataAdapter da;
        SqlCommand cmd;
        DataTable dt;
        public frmEducation()
        {
            InitializeComponent();
            dgvEducation.AutoGenerateColumns = false;
            DisplayData();
        }

        /*
         Associate's Degree (High Scool)
Bachelor's Degree
Master's Degree
Docteral Degree
Diploma
Certificate */
        private void DisplayData()
        {
            con.Open();
            da = new SqlDataAdapter(@"select id, education_name, (case when degree_level=0 then 'Associate''s Degree (High Scool)' 
            when degree_level=1 then 'Bachelor''s Degree' when degree_level=2 then 'Master''s Degree'
            when degree_level=3 then 'Docteral Degree' when degree_level=4 then 'Diploma' 
            when degree_level=5 then 'Certificate' end) As Degree, description from Education where is_deleted=0", con);
            dt = new DataTable();
            da.Fill(dt);
            dgvEducation.DataSource = dt;
            con.Close();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            con.Open();
            frmEducationRegister frmEduReg = new frmEducationRegister();
            frmEduReg.ShowDialog();
            con.Close();
            DisplayData();
        }

        private void dgvEducation_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dgvEducation.Rows[e.RowIndex].Cells["col_no"].Value = e.RowIndex + 1;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                int EduId = int.Parse(dgvEducation.SelectedRows[0].Cells["col_id"].Value.ToString());
                frmEducationRegister frmEduReg = new frmEducationRegister(EduId,
                dgvEducation.SelectedRows[0].Cells["col_education"].Value.ToString(),
                dgvEducation.SelectedRows[0].Cells["col_degree_level"].Value.ToString(),
                dgvEducation.SelectedRows[0].Cells["col_description"].Value.ToString());
                frmEduReg.ShowDialog();
                con.Close();
                DisplayData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please select data");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
                con.Open();
            try
            {
                DialogResult result = MessageBox.Show("Are you sure you want to delete?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    int eduId = int.Parse(dgvEducation.SelectedRows[0].Cells["col_id"].Value.ToString());
                    cmd = new SqlCommand("UPDATE Education SET  is_deleted=1 where id=@id", con);
                    cmd.Parameters.AddWithValue("@id", eduId);
                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Deleted Successfully!");

                }
                con.Close();
                DisplayData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please select data");
            }
        }

        private void frmEducation_Load(object sender, EventArgs e)
        {
            DisplayData();
        }

        

        
    }
}
