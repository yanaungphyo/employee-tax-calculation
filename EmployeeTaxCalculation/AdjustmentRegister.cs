﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace EmployeeTaxCalculation
{
    public partial class frmAdjustmentRegister : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        int EmpID;
        int adjYear_id;
        decimal d;
        
        
        public frmAdjustmentRegister()
        {
            InitializeComponent();
            txtTotalTax.ReadOnly = true;            
        }

        public frmAdjustmentRegister(int ID)
        {
            if (con.State != ConnectionState.Open)
                con.Open();            
            InitializeComponent();
            this.adjYear_id = ID;
            string query = @"select employee_name,adjustment_date, taxable_year, total_tax_amount, adjustment_amount 
            from Taxable_Yearly_Payment as y inner join Employee as e on e.id=y.employee_id where e.is_deleted=0 and y.is_deleted=0";
            cmd = new SqlCommand(query, con);
            SqlDataReader rdr = cmd.ExecuteReader();
            
            while (rdr.Read())
            {
                txtEmpName.Text = rdr.GetString(0);
                txtTotalTax.Text = rdr.GetDecimal(3).ToString();
                txtAdjAmt.Text = rdr.GetDecimal(4).ToString();
                dtpAdjDate.Value = rdr.GetDateTime(1);
                dtpAdjYear.Value = new DateTime(rdr.GetInt32(2), DateTime.Now.Month, DateTime.Now.Day);
                return;
            }
            

        }
        
        private void frmAdjustmentRegister_Load(object sender, EventArgs e)
        {
            /*
               if (con.State != ConnectionState.Open)
                   con.Open();
               dtpAdjYear.Value = DateTime.Today; 
               string query = "select id,employee_name from Employee where is_deleted=0";
               cmd = new SqlCommand(query, con);
           
               con.Open();
               SqlDataReader reader = cmd.ExecuteReader();
               AutoCompleteStringCollection MyCollection = new AutoCompleteStringCollection();
               while (reader.Read())
               {
                   MyCollection.Add(reader.GetString(0));
               }             
                txtEmpName.AutoCompleteCustomSource = MyCollection;
                txtEmpName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtEmpName.AutoCompleteSource = AutoCompleteSource.CustomSource;
                con.Close();
             */
            DataTable dt = new DataTable();
            
                con.Open();
                SqlDataAdapter adapt = new SqlDataAdapter("select id,employee_name from Employee where is_deleted=0", con);
                
                    adapt.SelectCommand.CommandTimeout = 120;
                    adapt.Fill(dt);
                
            

            //use LINQ method syntax to pull the Title field from a DT into a string array...
            string[] postSource = dt
                                .AsEnumerable()
                                .Select<System.Data.DataRow, String>(x => x.Field<String>("employee_name"))
                                .ToArray();

            var source = new AutoCompleteStringCollection();
            source.AddRange(postSource);  
            
            txtEmpName.AutoCompleteCustomSource = source;
            txtEmpName.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtEmpName.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)            
                con.Open();
                if (txtEmpName.Text == "")
                {
                    MessageBox.Show("Please enter Name!");
                    return;
                }
                if (!decimal.TryParse(txtAdjAmt.Text, out d) || Decimal.Parse(txtAdjAmt.Text) < 0)
                {
                    MessageBox.Show("Please enter Adjustment Amount!");
                    return;
                }
                string Emp = "select id from Employee where employee_name='" + txtEmpName.Text + "' and is_deleted=0";
                SqlCommand cmdEmp = new SqlCommand(Emp, con);
                SqlDataReader rdr1 = cmdEmp.ExecuteReader();

                while (rdr1.Read())
                {
                    EmpID = rdr1.GetInt32(0);
                }
                if (con.State != ConnectionState.Closed)
                    con.Close();
                if (con.State != ConnectionState.Open)
                    con.Open();                


                try
                {
                    string validationQuery = @"select y.id,employee_name,adjustment_date, taxable_year, total_tax_amount, 
                adjustment_amount from Taxable_Yearly_Payment as y inner join Employee as e on y.employee_id=e.id where e.is_deleted=0 and y.is_deleted=0 and employee_name='" + txtEmpName.Text + "' and taxable_year= " + dtpAdjYear.Value.Year;
                    cmd = new SqlCommand(validationQuery, con);
                    da = new SqlDataAdapter(cmd);

                    DataTable Validationdt = new DataTable();
                    da.Fill(Validationdt);
                    if (Validationdt.Rows.Count != 0)
                    {
                        MessageBox.Show(txtEmpName.Text + " is already calculated for " + dtpAdjYear.Value.Year);
                    }
                    else
                    {


                        string query = @"insert into Taxable_Yearly_Payment(employee_id, adjustment_date, taxable_year, total_tax_amount, adjustment_amount, is_deleted, created_date, updated_date) values
                (@employee_id, @adjustment_date, @taxable_year, @total_tax_amount, @adjustment_amount, @is_deleted, @created_date, @updated_date)";
                        cmd = new SqlCommand(query, con);
                        cmd.Parameters.AddWithValue("@employee_id", EmpID);
                        cmd.Parameters.AddWithValue("@adjustment_date", dtpAdjDate.Value.Date);
                        cmd.Parameters.AddWithValue("@taxable_year", dtpAdjYear.Value.Year);
                        cmd.Parameters.AddWithValue("@total_tax_amount", txtTotalTax.Text);
                        cmd.Parameters.AddWithValue("@adjustment_amount", txtAdjAmt.Text);
                        cmd.Parameters.AddWithValue("@is_deleted", Boolean.FalseString);
                        cmd.Parameters.AddWithValue("@created_date", DateTime.Now);
                        cmd.Parameters.AddWithValue("@updated_date", DateTime.Now);

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Success");

                        con.Close();
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please correct data");
                }
        }

        

        private void txtEmpName_Leave(object sender, EventArgs e)
        {
            
            autofill();
        }

        private void dtpAdjYear_ValueChanged(object sender, EventArgs e)
        {
            
            autofill();
            
        }

        private void autofill()
        {
            
            if (con.State != ConnectionState.Open)
                con.Open();
            string EmpName = txtEmpName.Text;
            int taxyear=dtpAdjYear.Value.Year;
            string str =@"select YearlyTotalTax from vw_yearly_total_tax where employee_name='"+EmpName+"'"+" and taxable_year="+taxyear ;
            
            cmd = new SqlCommand(str, con);
            con.Close();
            con.Open();
            SqlDataReader rdr3 = cmd.ExecuteReader();
            
            if (rdr3.Read())       
            {                
                    txtTotalTax.Text = rdr3.GetDecimal(0).ToString();              
            }
            else         
              
                    txtTotalTax.Text = "0.00";
            


            if (con.State != ConnectionState.Closed)
                con.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
                con.Open();
            if (txtEmpName.Text == "")
            {
                MessageBox.Show("Please enter Name!");
                return;
            }
            if (!decimal.TryParse(txtAdjAmt.Text, out d) || Decimal.Parse(txtAdjAmt.Text) < 0)
            {
                MessageBox.Show("Please enter Adjustment Amount!");
                return;
            }
            
            try
            {

                string queryEmployeeUpdate = @"Update Taxable_Yearly_Payment set adjustment_date=@adjustment_date, taxable_year=@taxable_year,
                                           total_tax_amount=@total_tax_amount, adjustment_amount=@adjustment_amount, updated_date=@updated_date where id=@id";
                cmd = new SqlCommand(queryEmployeeUpdate, con);
                cmd.Parameters.AddWithValue("@id", this.adjYear_id);
                cmd.Parameters.AddWithValue("@adjustment_date", dtpAdjDate.Value);
                cmd.Parameters.AddWithValue("@taxable_year", dtpAdjYear.Value.Year);
                cmd.Parameters.AddWithValue("@total_tax_amount", txtTotalTax.Text);
                cmd.Parameters.AddWithValue("@adjustment_amount", txtAdjAmt.Text);
                cmd.Parameters.AddWithValue("@updated_date", DateTime.Now);

                cmd.ExecuteNonQuery();
                MessageBox.Show("Updated Successfully");
                this.Close();
                if (con.State != ConnectionState.Closed)
                    con.Close();
            }
            catch (Exception ex)
            {
            }
        }
       
    }
}
