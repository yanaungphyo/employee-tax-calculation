﻿namespace EmployeeTaxCalculation
{
    partial class frmEmpBankAccHistoryReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.Emp_Bank_Acc_History_ReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.EmployeeTaxCalculationDataSet = new EmployeeTaxCalculation.EmployeeTaxCalculationDataSet();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rvEmpBankAccHistory = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cmbEmployee = new System.Windows.Forms.ComboBox();
            this.chkEmployee = new System.Windows.Forms.CheckBox();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.chkDepartment = new System.Windows.Forms.CheckBox();
            this.cmbBranch = new System.Windows.Forms.ComboBox();
            this.chkBranch = new System.Windows.Forms.CheckBox();
            this.cmbBank = new System.Windows.Forms.ComboBox();
            this.chkBank = new System.Windows.Forms.CheckBox();
            this.cmbSubsidiary = new System.Windows.Forms.ComboBox();
            this.chkSubsidiary = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.Emp_Bank_Acc_History_ReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeTaxCalculationDataSet)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Emp_Bank_Acc_History_ReportBindingSource
            // 
            this.Emp_Bank_Acc_History_ReportBindingSource.DataMember = "Emp_Bank_Acc_History_Report";
            this.Emp_Bank_Acc_History_ReportBindingSource.DataSource = this.EmployeeTaxCalculationDataSet;
            // 
            // EmployeeTaxCalculationDataSet
            // 
            this.EmployeeTaxCalculationDataSet.DataSetName = "EmployeeTaxCalculationDataSet";
            this.EmployeeTaxCalculationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 509);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rvEmpBankAccHistory);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 130);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(976, 379);
            this.panel3.TabIndex = 1;
            // 
            // rvEmpBankAccHistory
            // 
            this.rvEmpBankAccHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource2.Name = "Emp_Bank_Acc_History";
            reportDataSource2.Value = this.Emp_Bank_Acc_History_ReportBindingSource;
            this.rvEmpBankAccHistory.LocalReport.DataSources.Add(reportDataSource2);
            this.rvEmpBankAccHistory.LocalReport.ReportEmbeddedResource = "EmployeeTaxCalculation.EmpBankAccHistoryReport.rdlc";
            this.rvEmpBankAccHistory.Location = new System.Drawing.Point(0, 0);
            this.rvEmpBankAccHistory.Name = "rvEmpBankAccHistory";
            this.rvEmpBankAccHistory.Size = new System.Drawing.Size(976, 379);
            this.rvEmpBankAccHistory.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.cmbEmployee);
            this.panel2.Controls.Add(this.chkEmployee);
            this.panel2.Controls.Add(this.cmbDepartment);
            this.panel2.Controls.Add(this.chkDepartment);
            this.panel2.Controls.Add(this.cmbBranch);
            this.panel2.Controls.Add(this.chkBranch);
            this.panel2.Controls.Add(this.cmbBank);
            this.panel2.Controls.Add(this.chkBank);
            this.panel2.Controls.Add(this.cmbSubsidiary);
            this.panel2.Controls.Add(this.chkSubsidiary);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(976, 130);
            this.panel2.TabIndex = 0;
            // 
            // btnShowAll
            // 
            this.btnShowAll.Location = new System.Drawing.Point(109, 92);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(100, 30);
            this.btnShowAll.TabIndex = 11;
            this.btnShowAll.Text = "Show All";
            this.btnShowAll.UseVisualStyleBackColor = true;
            this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(3, 92);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(100, 30);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cmbEmployee
            // 
            this.cmbEmployee.FormattingEnabled = true;
            this.cmbEmployee.Location = new System.Drawing.Point(589, 19);
            this.cmbEmployee.Name = "cmbEmployee";
            this.cmbEmployee.Size = new System.Drawing.Size(150, 30);
            this.cmbEmployee.TabIndex = 9;
            // 
            // chkEmployee
            // 
            this.chkEmployee.AutoSize = true;
            this.chkEmployee.Location = new System.Drawing.Point(494, 21);
            this.chkEmployee.Name = "chkEmployee";
            this.chkEmployee.Size = new System.Drawing.Size(89, 28);
            this.chkEmployee.TabIndex = 8;
            this.chkEmployee.Text = "Employee";
            this.chkEmployee.UseVisualStyleBackColor = true;
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(386, 81);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(150, 30);
            this.cmbDepartment.TabIndex = 7;
            // 
            // chkDepartment
            // 
            this.chkDepartment.AutoSize = true;
            this.chkDepartment.Location = new System.Drawing.Point(278, 81);
            this.chkDepartment.Name = "chkDepartment";
            this.chkDepartment.Size = new System.Drawing.Size(102, 28);
            this.chkDepartment.TabIndex = 6;
            this.chkDepartment.Text = "Department";
            this.chkDepartment.UseVisualStyleBackColor = true;
            // 
            // cmbBranch
            // 
            this.cmbBranch.FormattingEnabled = true;
            this.cmbBranch.Location = new System.Drawing.Point(333, 20);
            this.cmbBranch.Name = "cmbBranch";
            this.cmbBranch.Size = new System.Drawing.Size(150, 30);
            this.cmbBranch.TabIndex = 5;
            // 
            // chkBranch
            // 
            this.chkBranch.AutoSize = true;
            this.chkBranch.Location = new System.Drawing.Point(256, 22);
            this.chkBranch.Name = "chkBranch";
            this.chkBranch.Size = new System.Drawing.Size(71, 28);
            this.chkBranch.TabIndex = 4;
            this.chkBranch.Text = "Branch";
            this.chkBranch.UseVisualStyleBackColor = true;
            // 
            // cmbBank
            // 
            this.cmbBank.FormattingEnabled = true;
            this.cmbBank.Location = new System.Drawing.Point(113, 81);
            this.cmbBank.Name = "cmbBank";
            this.cmbBank.Size = new System.Drawing.Size(150, 30);
            this.cmbBank.TabIndex = 3;
            // 
            // chkBank
            // 
            this.chkBank.AutoSize = true;
            this.chkBank.Location = new System.Drawing.Point(12, 82);
            this.chkBank.Name = "chkBank";
            this.chkBank.Size = new System.Drawing.Size(98, 28);
            this.chkBank.TabIndex = 2;
            this.chkBank.Text = "Bank Name";
            this.chkBank.UseVisualStyleBackColor = true;
            // 
            // cmbSubsidiary
            // 
            this.cmbSubsidiary.FormattingEnabled = true;
            this.cmbSubsidiary.Location = new System.Drawing.Point(100, 21);
            this.cmbSubsidiary.Name = "cmbSubsidiary";
            this.cmbSubsidiary.Size = new System.Drawing.Size(150, 30);
            this.cmbSubsidiary.TabIndex = 1;
            // 
            // chkSubsidiary
            // 
            this.chkSubsidiary.AutoSize = true;
            this.chkSubsidiary.Location = new System.Drawing.Point(12, 22);
            this.chkSubsidiary.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkSubsidiary.Name = "chkSubsidiary";
            this.chkSubsidiary.Size = new System.Drawing.Size(91, 28);
            this.chkSubsidiary.TabIndex = 0;
            this.chkSubsidiary.Text = "Subsidiary";
            this.chkSubsidiary.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnSearch);
            this.panel4.Controls.Add(this.btnShowAll);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(761, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(215, 130);
            this.panel4.TabIndex = 12;
            // 
            // frmEmpBankAccHistoryReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 509);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmEmpBankAccHistoryReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "EmpBankAccHistoryReport";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.EmpBankAccHistoryReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Emp_Bank_Acc_History_ReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeTaxCalculationDataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private Microsoft.Reporting.WinForms.ReportViewer rvEmpBankAccHistory;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox cmbEmployee;
        private System.Windows.Forms.CheckBox chkEmployee;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.CheckBox chkDepartment;
        private System.Windows.Forms.ComboBox cmbBranch;
        private System.Windows.Forms.CheckBox chkBranch;
        private System.Windows.Forms.ComboBox cmbBank;
        private System.Windows.Forms.CheckBox chkBank;
        private System.Windows.Forms.ComboBox cmbSubsidiary;
        private System.Windows.Forms.CheckBox chkSubsidiary;
        private System.Windows.Forms.BindingSource Emp_Bank_Acc_History_ReportBindingSource;
        private EmployeeTaxCalculationDataSet EmployeeTaxCalculationDataSet;
        private System.Windows.Forms.Panel panel4;
    }
}