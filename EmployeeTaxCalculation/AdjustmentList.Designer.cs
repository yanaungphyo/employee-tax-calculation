﻿namespace EmployeeTaxCalculation
{
    partial class frmAdjustmentList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cmbEmpName = new System.Windows.Forms.ComboBox();
            this.dtpToYear = new System.Windows.Forms.DateTimePicker();
            this.lblToYear = new System.Windows.Forms.Label();
            this.dtpFromYear = new System.Windows.Forms.DateTimePicker();
            this.chkYear = new System.Windows.Forms.CheckBox();
            this.chkEmpName = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvTaxableYear = new System.Windows.Forms.DataGridView();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_employee_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_employee_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_adj_year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_adj_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_total_tax_amt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_adj_amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTaxableYear)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.cmbEmpName);
            this.panel1.Controls.Add(this.dtpToYear);
            this.panel1.Controls.Add(this.lblToYear);
            this.panel1.Controls.Add(this.dtpFromYear);
            this.panel1.Controls.Add(this.chkYear);
            this.panel1.Controls.Add(this.chkEmpName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(643, 95);
            this.panel1.TabIndex = 0;
            // 
            // btnSearch
            // 
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.Location = new System.Drawing.Point(311, 50);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(100, 30);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cmbEmpName
            // 
            this.cmbEmpName.FormattingEnabled = true;
            this.cmbEmpName.Location = new System.Drawing.Point(137, 12);
            this.cmbEmpName.Name = "cmbEmpName";
            this.cmbEmpName.Size = new System.Drawing.Size(121, 30);
            this.cmbEmpName.TabIndex = 5;
            // 
            // dtpToYear
            // 
            this.dtpToYear.CustomFormat = "yyyy";
            this.dtpToYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpToYear.Location = new System.Drawing.Point(230, 50);
            this.dtpToYear.Name = "dtpToYear";
            this.dtpToYear.ShowUpDown = true;
            this.dtpToYear.Size = new System.Drawing.Size(75, 30);
            this.dtpToYear.TabIndex = 4;
            // 
            // lblToYear
            // 
            this.lblToYear.AutoSize = true;
            this.lblToYear.Location = new System.Drawing.Point(190, 52);
            this.lblToYear.Name = "lblToYear";
            this.lblToYear.Size = new System.Drawing.Size(26, 24);
            this.lblToYear.TabIndex = 3;
            this.lblToYear.Text = "To";
            // 
            // dtpFromYear
            // 
            this.dtpFromYear.CustomFormat = "yyyy";
            this.dtpFromYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromYear.Location = new System.Drawing.Point(100, 50);
            this.dtpFromYear.Name = "dtpFromYear";
            this.dtpFromYear.ShowUpDown = true;
            this.dtpFromYear.Size = new System.Drawing.Size(75, 30);
            this.dtpFromYear.TabIndex = 2;
            // 
            // chkYear
            // 
            this.chkYear.AutoSize = true;
            this.chkYear.Location = new System.Drawing.Point(3, 50);
            this.chkYear.Name = "chkYear";
            this.chkYear.Size = new System.Drawing.Size(91, 28);
            this.chkYear.TabIndex = 1;
            this.chkYear.Text = "Year From";
            this.chkYear.UseVisualStyleBackColor = true;
            // 
            // chkEmpName
            // 
            this.chkEmpName.AutoSize = true;
            this.chkEmpName.Location = new System.Drawing.Point(3, 14);
            this.chkEmpName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkEmpName.Name = "chkEmpName";
            this.chkEmpName.Size = new System.Drawing.Size(128, 28);
            this.chkEmpName.TabIndex = 0;
            this.chkEmpName.Text = "Employee Name";
            this.chkEmpName.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnDelete);
            this.panel2.Controls.Add(this.btnUpdate);
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 387);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(643, 48);
            this.panel2.TabIndex = 1;
            // 
            // btnDelete
            // 
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Location = new System.Drawing.Point(260, 8);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 30);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Location = new System.Drawing.Point(137, 8);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 30);
            this.btnUpdate.TabIndex = 1;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.Location = new System.Drawing.Point(12, 8);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnAdd.Size = new System.Drawing.Size(100, 30);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add New";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvTaxableYear);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 95);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(643, 292);
            this.panel3.TabIndex = 2;
            // 
            // dgvTaxableYear
            // 
            this.dgvTaxableYear.AllowUserToAddRows = false;
            this.dgvTaxableYear.AllowUserToDeleteRows = false;
            this.dgvTaxableYear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTaxableYear.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_id,
            this.col_no,
            this.col_employee_id,
            this.col_employee_name,
            this.col_adj_year,
            this.col_adj_date,
            this.col_total_tax_amt,
            this.col_adj_amount});
            this.dgvTaxableYear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTaxableYear.Location = new System.Drawing.Point(0, 0);
            this.dgvTaxableYear.MultiSelect = false;
            this.dgvTaxableYear.Name = "dgvTaxableYear";
            this.dgvTaxableYear.ReadOnly = true;
            this.dgvTaxableYear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTaxableYear.Size = new System.Drawing.Size(643, 292);
            this.dgvTaxableYear.TabIndex = 0;
            this.dgvTaxableYear.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvTaxableYear_RowPostPaint);
            // 
            // col_id
            // 
            this.col_id.DataPropertyName = "id";
            this.col_id.HeaderText = "Id";
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            this.col_id.Visible = false;
            // 
            // col_no
            // 
            this.col_no.HeaderText = "No";
            this.col_no.Name = "col_no";
            this.col_no.ReadOnly = true;
            // 
            // col_employee_id
            // 
            this.col_employee_id.DataPropertyName = "employee_id";
            this.col_employee_id.HeaderText = "employee_id";
            this.col_employee_id.Name = "col_employee_id";
            this.col_employee_id.ReadOnly = true;
            this.col_employee_id.Visible = false;
            // 
            // col_employee_name
            // 
            this.col_employee_name.DataPropertyName = "employee_name";
            this.col_employee_name.HeaderText = "Employee Name";
            this.col_employee_name.Name = "col_employee_name";
            this.col_employee_name.ReadOnly = true;
            // 
            // col_adj_year
            // 
            this.col_adj_year.DataPropertyName = "taxable_year";
            this.col_adj_year.HeaderText = "Adjustment Year";
            this.col_adj_year.Name = "col_adj_year";
            this.col_adj_year.ReadOnly = true;
            // 
            // col_adj_date
            // 
            this.col_adj_date.DataPropertyName = "adjustment_date";
            this.col_adj_date.HeaderText = "Adjustment Date";
            this.col_adj_date.Name = "col_adj_date";
            this.col_adj_date.ReadOnly = true;
            // 
            // col_total_tax_amt
            // 
            this.col_total_tax_amt.DataPropertyName = "total_tax_amount";
            this.col_total_tax_amt.HeaderText = "Total Tax Amount";
            this.col_total_tax_amt.Name = "col_total_tax_amt";
            this.col_total_tax_amt.ReadOnly = true;
            // 
            // col_adj_amount
            // 
            this.col_adj_amount.DataPropertyName = "adjustment_amount";
            this.col_adj_amount.HeaderText = "Adjustment Amount";
            this.col_adj_amount.Name = "col_adj_amount";
            this.col_adj_amount.ReadOnly = true;
            // 
            // frmAdjustmentList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 435);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmAdjustmentList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdjustmentList";
            this.Load += new System.EventHandler(this.frmAdjustmentList_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTaxableYear)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpToYear;
        private System.Windows.Forms.Label lblToYear;
        private System.Windows.Forms.DateTimePicker dtpFromYear;
        private System.Windows.Forms.CheckBox chkYear;
        private System.Windows.Forms.CheckBox chkEmpName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgvTaxableYear;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox cmbEmpName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_employee_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_employee_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_adj_year;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_adj_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_total_tax_amt;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_adj_amount;
    }
}