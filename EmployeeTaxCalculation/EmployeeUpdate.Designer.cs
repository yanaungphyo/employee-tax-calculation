﻿namespace EmployeeTaxCalculation
{
    partial class frmEmployeeUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblMonthlyNetSalary = new System.Windows.Forms.Label();
            this.lblNoOfParents = new System.Windows.Forms.Label();
            this.lblSpouse = new System.Windows.Forms.Label();
            this.lblNoOfChildren = new System.Windows.Forms.Label();
            this.txtEmployeeName = new System.Windows.Forms.TextBox();
            this.txtMonthlyNetSalary = new System.Windows.Forms.TextBox();
            this.txtNoOfParents = new System.Windows.Forms.TextBox();
            this.txtSpouse = new System.Windows.Forms.TextBox();
            this.txtNoOfChildren = new System.Windows.Forms.TextBox();
            this.btnEmployeeUpdate = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNRC = new System.Windows.Forms.TextBox();
            this.lblNRC = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.Location = new System.Drawing.Point(17, 17);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(105, 24);
            this.lblEmployeeName.TabIndex = 0;
            this.lblEmployeeName.Text = "EmployeeName";
            // 
            // lblMonthlyNetSalary
            // 
            this.lblMonthlyNetSalary.AutoSize = true;
            this.lblMonthlyNetSalary.Location = new System.Drawing.Point(17, 99);
            this.lblMonthlyNetSalary.Name = "lblMonthlyNetSalary";
            this.lblMonthlyNetSalary.Size = new System.Drawing.Size(126, 24);
            this.lblMonthlyNetSalary.TabIndex = 1;
            this.lblMonthlyNetSalary.Text = "Monthly Net Salary";
            // 
            // lblNoOfParents
            // 
            this.lblNoOfParents.AutoSize = true;
            this.lblNoOfParents.Location = new System.Drawing.Point(17, 138);
            this.lblNoOfParents.Name = "lblNoOfParents";
            this.lblNoOfParents.Size = new System.Drawing.Size(126, 24);
            this.lblNoOfParents.TabIndex = 2;
            this.lblNoOfParents.Text = "Number Of Parents";
            // 
            // lblSpouse
            // 
            this.lblSpouse.AutoSize = true;
            this.lblSpouse.Location = new System.Drawing.Point(17, 178);
            this.lblSpouse.Name = "lblSpouse";
            this.lblSpouse.Size = new System.Drawing.Size(54, 24);
            this.lblSpouse.TabIndex = 3;
            this.lblSpouse.Text = "Spouse";
            // 
            // lblNoOfChildren
            // 
            this.lblNoOfChildren.AutoSize = true;
            this.lblNoOfChildren.Location = new System.Drawing.Point(17, 217);
            this.lblNoOfChildren.Name = "lblNoOfChildren";
            this.lblNoOfChildren.Size = new System.Drawing.Size(131, 24);
            this.lblNoOfChildren.TabIndex = 4;
            this.lblNoOfChildren.Text = "Number Of Children";
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Location = new System.Drawing.Point(151, 11);
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Size = new System.Drawing.Size(150, 30);
            this.txtEmployeeName.TabIndex = 5;
            // 
            // txtMonthlyNetSalary
            // 
            this.txtMonthlyNetSalary.Location = new System.Drawing.Point(151, 96);
            this.txtMonthlyNetSalary.Name = "txtMonthlyNetSalary";
            this.txtMonthlyNetSalary.Size = new System.Drawing.Size(150, 30);
            this.txtMonthlyNetSalary.TabIndex = 6;
            this.txtMonthlyNetSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonthlyNetSalary_KeyPress);
            // 
            // txtNoOfParents
            // 
            this.txtNoOfParents.Location = new System.Drawing.Point(151, 135);
            this.txtNoOfParents.Name = "txtNoOfParents";
            this.txtNoOfParents.Size = new System.Drawing.Size(150, 30);
            this.txtNoOfParents.TabIndex = 7;
            // 
            // txtSpouse
            // 
            this.txtSpouse.Location = new System.Drawing.Point(151, 175);
            this.txtSpouse.Name = "txtSpouse";
            this.txtSpouse.Size = new System.Drawing.Size(150, 30);
            this.txtSpouse.TabIndex = 8;
            // 
            // txtNoOfChildren
            // 
            this.txtNoOfChildren.Location = new System.Drawing.Point(151, 214);
            this.txtNoOfChildren.Name = "txtNoOfChildren";
            this.txtNoOfChildren.Size = new System.Drawing.Size(150, 30);
            this.txtNoOfChildren.TabIndex = 9;
            // 
            // btnEmployeeUpdate
            // 
            this.btnEmployeeUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEmployeeUpdate.Location = new System.Drawing.Point(104, 256);
            this.btnEmployeeUpdate.Name = "btnEmployeeUpdate";
            this.btnEmployeeUpdate.Size = new System.Drawing.Size(100, 50);
            this.btnEmployeeUpdate.TabIndex = 10;
            this.btnEmployeeUpdate.Text = "Update";
            this.btnEmployeeUpdate.UseVisualStyleBackColor = true;
            this.btnEmployeeUpdate.Click += new System.EventHandler(this.btnEmployeeUpdate_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtNRC);
            this.panel1.Controls.Add(this.lblNRC);
            this.panel1.Controls.Add(this.txtSpouse);
            this.panel1.Controls.Add(this.btnEmployeeUpdate);
            this.panel1.Controls.Add(this.lblEmployeeName);
            this.panel1.Controls.Add(this.txtNoOfChildren);
            this.panel1.Controls.Add(this.lblMonthlyNetSalary);
            this.panel1.Controls.Add(this.lblNoOfParents);
            this.panel1.Controls.Add(this.txtNoOfParents);
            this.panel1.Controls.Add(this.lblSpouse);
            this.panel1.Controls.Add(this.txtMonthlyNetSalary);
            this.panel1.Controls.Add(this.lblNoOfChildren);
            this.panel1.Controls.Add(this.txtEmployeeName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(320, 324);
            this.panel1.TabIndex = 11;
            // 
            // txtNRC
            // 
            this.txtNRC.Location = new System.Drawing.Point(151, 52);
            this.txtNRC.Name = "txtNRC";
            this.txtNRC.Size = new System.Drawing.Size(150, 30);
            this.txtNRC.TabIndex = 12;
            this.txtNRC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNRC_KeyPress);
            // 
            // lblNRC
            // 
            this.lblNRC.AutoSize = true;
            this.lblNRC.Location = new System.Drawing.Point(17, 58);
            this.lblNRC.Name = "lblNRC";
            this.lblNRC.Size = new System.Drawing.Size(59, 24);
            this.lblNRC.TabIndex = 11;
            this.lblNRC.Text = "NRC_No";
            // 
            // frmEmployeeUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 324);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmEmployeeUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmployeeUpdate";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblMonthlyNetSalary;
        private System.Windows.Forms.Label lblNoOfParents;
        private System.Windows.Forms.Label lblSpouse;
        private System.Windows.Forms.Label lblNoOfChildren;
        private System.Windows.Forms.TextBox txtEmployeeName;
        private System.Windows.Forms.TextBox txtMonthlyNetSalary;
        private System.Windows.Forms.TextBox txtNoOfParents;
        private System.Windows.Forms.TextBox txtSpouse;
        private System.Windows.Forms.TextBox txtNoOfChildren;
        private System.Windows.Forms.Button btnEmployeeUpdate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtNRC;
        private System.Windows.Forms.Label lblNRC;
    }
}