﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmEmployeeRegister : Form
    {
        
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        decimal d;
        int i;
        SqlDataAdapter da;
        DataTable dt;
        public frmEmployeeRegister()
        {
            InitializeComponent();
        }

        private void btnEmployeeRegister_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)
            con.Open();

            string queryNRC = "select NRC_No from Employee where is_deleted=0 and NRC_No='" + txtNRC.Text+"'";
            da = new SqlDataAdapter(queryNRC, con);
            dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("NRC_No is already added");
                return;
            }
            else
            {

                if (txtEmployeeName.Text == "")
                {
                    MessageBox.Show("Please enter Name!");
                    return;
                }
                if (txtNRC.Text == "")
                {
                    MessageBox.Show("Please enter NRC No.");
                }
                if (!decimal.TryParse(txtMonthlyNetSalary.Text, out d) || Decimal.Parse(txtMonthlyNetSalary.Text) < 0)
                {
                    MessageBox.Show("Please enter Monthly Net Salary");
                    return;
                }
                if (!int.TryParse(txtNoOfParents.Text, out i) || Int32.Parse(txtNoOfParents.Text) < 0 || Int32.Parse(txtNoOfParents.Text) > 2)
                {
                    MessageBox.Show("Please enter Parent Numbers");
                    return;
                }
                if (!int.TryParse(txtSpouse.Text, out i) || Convert.ToInt32(txtSpouse.Text) < 0 || Convert.ToInt32(txtSpouse.Text) > 1)
                {
                    MessageBox.Show("Please enter Spouse (eg..0,1)");
                    return;
                }
                if (!int.TryParse(txtNoOfChildren.Text, out i) || Int32.Parse(txtNoOfChildren.Text) < 0)
                {
                    MessageBox.Show("Please enter Children (eg..0,1,2,etc...)");
                    return;
                }

                SqlCommand cmd = new SqlCommand(@"INSERT into Employee( employee_name, NRC_No, monthly_net_salary, no_of_parents, no_of_spouse, no_of_children, is_deleted, created_date, updated_date)
                    values ( @employee_name, @NRC_No, @monthly_net_salary, @no_of_parents, @no_of_spouse, @no_of_children, @is_deleted, @created_date, @updated_date)", con);
                cmd.Parameters.AddWithValue("@employee_name", txtEmployeeName.Text);
                cmd.Parameters.AddWithValue("@NRC_No", txtNRC.Text);
                cmd.Parameters.AddWithValue("@monthly_net_salary", Decimal.Parse(txtMonthlyNetSalary.Text));
                cmd.Parameters.AddWithValue("@no_of_Parents", Int32.Parse(txtNoOfParents.Text));
                cmd.Parameters.AddWithValue("@no_of_spouse", Int32.Parse(txtSpouse.Text));
                cmd.Parameters.AddWithValue("@no_of_Children", Int32.Parse(txtNoOfChildren.Text));
                cmd.Parameters.AddWithValue("@is_deleted", Boolean.FalseString);
                cmd.Parameters.AddWithValue("@created_date", DateTime.Now);
                cmd.Parameters.AddWithValue("@updated_date", DateTime.Now);
                cmd.ExecuteNonQuery();

                Clear();

                MessageBox.Show("Registration Succeed");
                this.Close();
                con.Close();
            }
        }
        void Clear()
        {
            txtEmployeeName.Text = "";
            txtNRC.Text = "";
            txtMonthlyNetSalary.Text = "";
            txtNoOfParents.Text = "";
            txtNoOfChildren.Text = "";
            txtSpouse.Text = "";
        }

        private void txtMonthlyNetSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void txtNoOfParents_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void txtSpouse_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void txtNoOfChildren_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void txtNRC_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space);
        }
    }
}
