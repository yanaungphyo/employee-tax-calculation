﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmEmployeeList : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlDataAdapter daEmployee;
        SqlCommand cmdEmployeeList;      
        
        public frmEmployeeList()
        {
            InitializeComponent();
            dgvEmpList.AutoGenerateColumns = false;
            DisplayData();
        }       

        private void btnEmpoloyeeListRegister_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)
            con.Open();
            frmEmployeeRegister frmEmployeeRegister = new frmEmployeeRegister();
            frmEmployeeRegister.ShowDialog();      
            
            con.Close();
            DisplayData();
        }

        private void btnEmployeeListUpdate_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            con.Open();
            try
            {
                int EmpID = int.Parse(dgvEmpList.SelectedRows[0].Cells["col_EmpID"].Value.ToString());
                frmEmployeeUpdate frmEmployeeUpdate = new frmEmployeeUpdate(EmpID,
                dgvEmpList.SelectedRows[0].Cells["col_EmpName"].Value.ToString(),
                dgvEmpList.SelectedRows[0].Cells["col_NRC"].Value.ToString(),
                dgvEmpList.SelectedRows[0].Cells["col_monthly_net_salary"].Value.ToString(),
                dgvEmpList.SelectedRows[0].Cells["col_no_of_parents"].Value.ToString(),
                dgvEmpList.SelectedRows[0].Cells["col_no_of_spouse"].Value.ToString(),
                dgvEmpList.SelectedRows[0].Cells["col_no_of_children"].Value.ToString());
                frmEmployeeUpdate.ShowDialog();
                con.Close();
                DisplayData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please select Employee");
            }
        }

        private void btnEmployeeListDelete_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
            con.Open();
            try
            {
                DialogResult result = MessageBox.Show("Are you sure you want to delete?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    int EmpID = int.Parse(dgvEmpList.SelectedRows[0].Cells["col_EmpID"].Value.ToString());
                    cmdEmployeeList = new SqlCommand("UPDATE Employee SET  is_deleted=1 where id=@id", con);
                    cmdEmployeeList.Parameters.AddWithValue("@id", EmpID);
                    cmdEmployeeList.ExecuteNonQuery();
                    MessageBox.Show("Deleted Successfully!");
                }
                con.Close();
                DisplayData();
            }
            catch (Exception ex)
            { MessageBox.Show("Please select Employee"); }
        }

        private void DisplayData()
         {
            con.Open();
            DataTable dt = new DataTable();
            daEmployee = new SqlDataAdapter("select * from Employee where is_deleted=0", con);
            daEmployee.Fill(dt);
            dgvEmpList.DataSource = dt;
            con.Close();
        }

        private void dgvEmpList_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dgvEmpList.Rows[e.RowIndex].Cells["col_No"].Value = e.RowIndex + 1;
        }

           
    }
}
