﻿namespace EmployeeTaxCalculation
{
    partial class frmAdjustmentRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmpName = new System.Windows.Forms.Label();
            this.lblAdjYear = new System.Windows.Forms.Label();
            this.lblTotalTax = new System.Windows.Forms.Label();
            this.lblAdjAmt = new System.Windows.Forms.Label();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.txtTotalTax = new System.Windows.Forms.TextBox();
            this.txtAdjAmt = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.dtpAdjYear = new System.Windows.Forms.DateTimePicker();
            this.lblAdjDate = new System.Windows.Forms.Label();
            this.dtpAdjDate = new System.Windows.Forms.DateTimePicker();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEmpName
            // 
            this.lblEmpName.AutoSize = true;
            this.lblEmpName.Location = new System.Drawing.Point(14, 15);
            this.lblEmpName.Name = "lblEmpName";
            this.lblEmpName.Size = new System.Drawing.Size(109, 24);
            this.lblEmpName.TabIndex = 0;
            this.lblEmpName.Text = "Employee Name";
            // 
            // lblAdjYear
            // 
            this.lblAdjYear.AutoSize = true;
            this.lblAdjYear.Location = new System.Drawing.Point(13, 90);
            this.lblAdjYear.Name = "lblAdjYear";
            this.lblAdjYear.Size = new System.Drawing.Size(110, 24);
            this.lblAdjYear.TabIndex = 1;
            this.lblAdjYear.Text = "Adjustment Year";
            // 
            // lblTotalTax
            // 
            this.lblTotalTax.AutoSize = true;
            this.lblTotalTax.Location = new System.Drawing.Point(14, 125);
            this.lblTotalTax.Name = "lblTotalTax";
            this.lblTotalTax.Size = new System.Drawing.Size(67, 24);
            this.lblTotalTax.TabIndex = 2;
            this.lblTotalTax.Text = "Total Tax";
            // 
            // lblAdjAmt
            // 
            this.lblAdjAmt.AutoSize = true;
            this.lblAdjAmt.Location = new System.Drawing.Point(14, 165);
            this.lblAdjAmt.Name = "lblAdjAmt";
            this.lblAdjAmt.Size = new System.Drawing.Size(133, 24);
            this.lblAdjAmt.TabIndex = 3;
            this.lblAdjAmt.Text = "Adjustment Amount";
            // 
            // txtEmpName
            // 
            this.txtEmpName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtEmpName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtEmpName.Location = new System.Drawing.Point(152, 12);
            this.txtEmpName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Size = new System.Drawing.Size(116, 30);
            this.txtEmpName.TabIndex = 4;
            this.txtEmpName.Leave += new System.EventHandler(this.txtEmpName_Leave);
            // 
            // txtTotalTax
            // 
            this.txtTotalTax.Location = new System.Drawing.Point(152, 122);
            this.txtTotalTax.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtTotalTax.Name = "txtTotalTax";
            this.txtTotalTax.Size = new System.Drawing.Size(116, 30);
            this.txtTotalTax.TabIndex = 6;
            // 
            // txtAdjAmt
            // 
            this.txtAdjAmt.Location = new System.Drawing.Point(152, 162);
            this.txtAdjAmt.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtAdjAmt.Name = "txtAdjAmt";
            this.txtAdjAmt.Size = new System.Drawing.Size(116, 30);
            this.txtAdjAmt.TabIndex = 7;
            // 
            // btnSave
            // 
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Location = new System.Drawing.Point(23, 204);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dtpAdjYear
            // 
            this.dtpAdjYear.CustomFormat = "yyyy";
            this.dtpAdjYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAdjYear.Location = new System.Drawing.Point(152, 84);
            this.dtpAdjYear.Name = "dtpAdjYear";
            this.dtpAdjYear.ShowUpDown = true;
            this.dtpAdjYear.Size = new System.Drawing.Size(116, 30);
            this.dtpAdjYear.TabIndex = 9;
            this.dtpAdjYear.ValueChanged += new System.EventHandler(this.dtpAdjYear_ValueChanged);
            // 
            // lblAdjDate
            // 
            this.lblAdjDate.AutoSize = true;
            this.lblAdjDate.Location = new System.Drawing.Point(13, 56);
            this.lblAdjDate.Name = "lblAdjDate";
            this.lblAdjDate.Size = new System.Drawing.Size(112, 24);
            this.lblAdjDate.TabIndex = 10;
            this.lblAdjDate.Text = "Adjustment Date";
            // 
            // dtpAdjDate
            // 
            this.dtpAdjDate.CustomFormat = "dd/MMM/yyyy";
            this.dtpAdjDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAdjDate.Location = new System.Drawing.Point(152, 50);
            this.dtpAdjDate.Name = "dtpAdjDate";
            this.dtpAdjDate.Size = new System.Drawing.Size(116, 30);
            this.dtpAdjDate.TabIndex = 11;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Location = new System.Drawing.Point(152, 204);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 30);
            this.btnUpdate.TabIndex = 12;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblAdjDate);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.lblEmpName);
            this.panel1.Controls.Add(this.dtpAdjDate);
            this.panel1.Controls.Add(this.lblAdjYear);
            this.panel1.Controls.Add(this.lblTotalTax);
            this.panel1.Controls.Add(this.dtpAdjYear);
            this.panel1.Controls.Add(this.lblAdjAmt);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.txtEmpName);
            this.panel1.Controls.Add(this.txtAdjAmt);
            this.panel1.Controls.Add(this.txtTotalTax);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(285, 261);
            this.panel1.TabIndex = 13;
            // 
            // frmAdjustmentRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 261);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmAdjustmentRegister";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdjustmentRegister";
            this.Load += new System.EventHandler(this.frmAdjustmentRegister_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblEmpName;
        private System.Windows.Forms.Label lblAdjYear;
        private System.Windows.Forms.Label lblTotalTax;
        private System.Windows.Forms.Label lblAdjAmt;
        private System.Windows.Forms.TextBox txtEmpName;
        private System.Windows.Forms.TextBox txtTotalTax;
        private System.Windows.Forms.TextBox txtAdjAmt;
        public System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DateTimePicker dtpAdjYear;
        private System.Windows.Forms.Label lblAdjDate;
        private System.Windows.Forms.DateTimePicker dtpAdjDate;
        public System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Panel panel1;
    }
}