﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmEducationRegister : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmdEduReg;        
        int EducationId;
        public frmEducationRegister()
        {
            InitializeComponent();
            if (string.IsNullOrWhiteSpace(cmbDegree.Text))
            {
                cmbDegree.Text = "--Please select--";
            }
        }

        public frmEducationRegister(int EduId, string Education, string Degree_Level, string Description)
        {
            InitializeComponent();
            if (string.IsNullOrWhiteSpace(cmbDegree.Text))
            {
                cmbDegree.Text = "--Please select--";
            }
            this.EducationId = EduId;
            txtEducation.Text = Education;
            cmbDegree.SelectedItem = Degree_Level;
            txtDescription.Text = Description;


        }

        

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (con.State != ConnectionState.Open)
                con.Open();
            if (EducationId == 0)
            {
                if (txtEducation.Text == "" || txtDescription.Text == "")
                {
                    MessageBox.Show("Please fill data");
                    return;
                }
                if (cmbDegree.SelectedItem == null)
                {
                    MessageBox.Show("Please select Degree Level");
                    return;
                }
                
                cmdEduReg = new SqlCommand(@"insert into Education(education_name, degree_level, description, is_deleted, created_date, updated_date) values
                (@education_name, @degree_level, @description, @is_deleted, @created_date, @updated_date)", con);

                cmdEduReg.Parameters.AddWithValue("@education_name", txtEducation.Text);
                cmdEduReg.Parameters.AddWithValue("@degree_level", cmbDegree.SelectedIndex);
                cmdEduReg.Parameters.AddWithValue("@description", txtDescription.Text);
                cmdEduReg.Parameters.AddWithValue("@is_deleted", bool.FalseString);
                cmdEduReg.Parameters.AddWithValue("@created_date", DateTime.Now);
                cmdEduReg.Parameters.AddWithValue("@updated_date", DateTime.Now);
                cmdEduReg.ExecuteNonQuery();
                MessageBox.Show("Saved Successfully");
                con.Close();
                Clear();
                this.Close();
                
            }
            else
            {
                if (txtEducation.Text == "" || txtDescription.Text =="")
                {
                    MessageBox.Show("Please fill data");
                    return;
                }
                cmdEduReg=new SqlCommand(@"update Education set education_name=@education_name, degree_level=@degree_level, description=@description, updated_date=@updated_date where id=@id",con);
                cmdEduReg.Parameters.AddWithValue("@id", this.EducationId);
                cmdEduReg.Parameters.AddWithValue("@education_name", txtEducation.Text);
                cmdEduReg.Parameters.AddWithValue("@degree_level", cmbDegree.SelectedIndex);
                cmdEduReg.Parameters.AddWithValue("@description", txtDescription.Text);
                cmdEduReg.Parameters.AddWithValue("@updated_date", DateTime.Now);
                cmdEduReg.ExecuteNonQuery();
                MessageBox.Show("Updated Successfully");
                con.Close();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void Clear()
        {
            txtEducation.Text = "";
            txtDescription.Text = "";            
        }
    }
}
