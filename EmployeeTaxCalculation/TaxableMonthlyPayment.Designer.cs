﻿namespace EmployeeTaxCalculation
{
    partial class frmTaxableMonthlyPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.chkEmployee = new System.Windows.Forms.CheckBox();
            this.cmbEmployee = new System.Windows.Forms.ComboBox();
            this.lblMonth = new System.Windows.Forms.Label();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.dgvMonthly = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.pbMonthlyCalculate = new System.Windows.Forms.ProgressBar();
            this.bgwMonthlyCalculate = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_empid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_empname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_monthly_salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_annual_salary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_personal_relief = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_parents_relief = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_spouse_relief = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_child_relief = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_other = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_taxable_income = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_0_20Lakh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_20Lakh_50Lakh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_50Lakh_100Lakh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_100Lakh_200Lakh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_200Lakh_300Lakh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_300Lakh_above = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_total_tax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_monthly_tax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonthly)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkEmployee
            // 
            this.chkEmployee.AutoSize = true;
            this.chkEmployee.Location = new System.Drawing.Point(12, 14);
            this.chkEmployee.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkEmployee.Name = "chkEmployee";
            this.chkEmployee.Size = new System.Drawing.Size(89, 28);
            this.chkEmployee.TabIndex = 0;
            this.chkEmployee.Text = "Employee";
            this.chkEmployee.UseVisualStyleBackColor = true;
            // 
            // cmbEmployee
            // 
            this.cmbEmployee.FormattingEnabled = true;
            this.cmbEmployee.Location = new System.Drawing.Point(107, 14);
            this.cmbEmployee.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cmbEmployee.Name = "cmbEmployee";
            this.cmbEmployee.Size = new System.Drawing.Size(140, 30);
            this.cmbEmployee.TabIndex = 1;
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.Location = new System.Drawing.Point(253, 16);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(49, 24);
            this.lblMonth.TabIndex = 2;
            this.lblMonth.Text = "Month";
            // 
            // dtpMonth
            // 
            this.dtpMonth.CustomFormat = "MMM/yyyy";
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Location = new System.Drawing.Point(308, 14);
            this.dtpMonth.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ShowUpDown = true;
            this.dtpMonth.Size = new System.Drawing.Size(98, 30);
            this.dtpMonth.TabIndex = 3;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCalculate.Location = new System.Drawing.Point(412, 14);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(100, 30);
            this.btnCalculate.TabIndex = 4;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // dgvMonthly
            // 
            this.dgvMonthly.AllowUserToAddRows = false;
            this.dgvMonthly.AllowUserToDeleteRows = false;
            this.dgvMonthly.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMonthly.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_id,
            this.col_no,
            this.col_empid,
            this.col_empname,
            this.col_monthly_salary,
            this.col_annual_salary,
            this.col_personal_relief,
            this.col_parents_relief,
            this.col_spouse_relief,
            this.col_child_relief,
            this.col_other,
            this.col_taxable_income,
            this.col_0_20Lakh,
            this.col_20Lakh_50Lakh,
            this.col_50Lakh_100Lakh,
            this.col_100Lakh_200Lakh,
            this.col_200Lakh_300Lakh,
            this.col_300Lakh_above,
            this.col_total_tax,
            this.col_monthly_tax});
            this.dgvMonthly.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMonthly.Location = new System.Drawing.Point(0, 0);
            this.dgvMonthly.MultiSelect = false;
            this.dgvMonthly.Name = "dgvMonthly";
            this.dgvMonthly.ReadOnly = true;
            this.dgvMonthly.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMonthly.Size = new System.Drawing.Size(1054, 582);
            this.dgvMonthly.TabIndex = 5;
            this.dgvMonthly.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvMonthly_RowPostPaint);
            // 
            // btnSave
            // 
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Location = new System.Drawing.Point(38, 9);
            this.btnSave.Name = "btnSave";
            this.btnSave.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pbMonthlyCalculate
            // 
            this.pbMonthlyCalculate.Location = new System.Drawing.Point(3, 9);
            this.pbMonthlyCalculate.Name = "pbMonthlyCalculate";
            this.pbMonthlyCalculate.Size = new System.Drawing.Size(150, 30);
            this.pbMonthlyCalculate.TabIndex = 7;
            // 
            // bgwMonthlyCalculate
            // 
            this.bgwMonthlyCalculate.WorkerReportsProgress = true;
            this.bgwMonthlyCalculate.WorkerSupportsCancellation = true;
            this.bgwMonthlyCalculate.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwMonthlyCalculate_DoWork);
            this.bgwMonthlyCalculate.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwMonthlyCalculate_ProgressChanged);
            this.bgwMonthlyCalculate.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwMonthlyCalculate_RunWorkerCompleted);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkEmployee);
            this.panel1.Controls.Add(this.cmbEmployee);
            this.panel1.Controls.Add(this.lblMonth);
            this.panel1.Controls.Add(this.dtpMonth);
            this.panel1.Controls.Add(this.btnCalculate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1054, 48);
            this.panel1.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.pbMonthlyCalculate);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 630);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1054, 51);
            this.panel2.TabIndex = 9;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnSave);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(904, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(150, 51);
            this.panel4.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvMonthly);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 48);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1054, 582);
            this.panel3.TabIndex = 10;
            // 
            // col_id
            // 
            this.col_id.DataPropertyName = "id";
            this.col_id.HeaderText = "Id";
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            this.col_id.Visible = false;
            // 
            // col_no
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_no.DefaultCellStyle = dataGridViewCellStyle1;
            this.col_no.HeaderText = "No";
            this.col_no.Name = "col_no";
            this.col_no.ReadOnly = true;
            // 
            // col_empid
            // 
            this.col_empid.DataPropertyName = "EmpID";
            this.col_empid.HeaderText = "Employee Id";
            this.col_empid.Name = "col_empid";
            this.col_empid.ReadOnly = true;
            this.col_empid.Visible = false;
            // 
            // col_empname
            // 
            this.col_empname.DataPropertyName = "employee_name";
            this.col_empname.HeaderText = "Employee Name";
            this.col_empname.Name = "col_empname";
            this.col_empname.ReadOnly = true;
            // 
            // col_monthly_salary
            // 
            this.col_monthly_salary.DataPropertyName = "monthly_salary";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_monthly_salary.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_monthly_salary.HeaderText = "Monthly Salary";
            this.col_monthly_salary.Name = "col_monthly_salary";
            this.col_monthly_salary.ReadOnly = true;
            // 
            // col_annual_salary
            // 
            this.col_annual_salary.DataPropertyName = "annual_salary";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_annual_salary.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_annual_salary.HeaderText = "Annual Salary";
            this.col_annual_salary.Name = "col_annual_salary";
            this.col_annual_salary.ReadOnly = true;
            // 
            // col_personal_relief
            // 
            this.col_personal_relief.DataPropertyName = "personal_relief";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.col_personal_relief.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_personal_relief.HeaderText = "Personal Relief";
            this.col_personal_relief.Name = "col_personal_relief";
            this.col_personal_relief.ReadOnly = true;
            // 
            // col_parents_relief
            // 
            this.col_parents_relief.DataPropertyName = "parent_relief";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_parents_relief.DefaultCellStyle = dataGridViewCellStyle5;
            this.col_parents_relief.HeaderText = "Parent Relief";
            this.col_parents_relief.Name = "col_parents_relief";
            this.col_parents_relief.ReadOnly = true;
            // 
            // col_spouse_relief
            // 
            this.col_spouse_relief.DataPropertyName = "spouse_relief";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_spouse_relief.DefaultCellStyle = dataGridViewCellStyle6;
            this.col_spouse_relief.HeaderText = "Spouse";
            this.col_spouse_relief.Name = "col_spouse_relief";
            this.col_spouse_relief.ReadOnly = true;
            // 
            // col_child_relief
            // 
            this.col_child_relief.DataPropertyName = "child_relief";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_child_relief.DefaultCellStyle = dataGridViewCellStyle7;
            this.col_child_relief.HeaderText = "Children Relief";
            this.col_child_relief.Name = "col_child_relief";
            this.col_child_relief.ReadOnly = true;
            // 
            // col_other
            // 
            this.col_other.HeaderText = "Other Deductible";
            this.col_other.Name = "col_other";
            this.col_other.ReadOnly = true;
            // 
            // col_taxable_income
            // 
            this.col_taxable_income.DataPropertyName = "taxableIncome";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_taxable_income.DefaultCellStyle = dataGridViewCellStyle8;
            this.col_taxable_income.HeaderText = "Taxable Income";
            this.col_taxable_income.Name = "col_taxable_income";
            this.col_taxable_income.ReadOnly = true;
            // 
            // col_0_20Lakh
            // 
            this.col_0_20Lakh.DataPropertyName = "0.00~2000000.00";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_0_20Lakh.DefaultCellStyle = dataGridViewCellStyle9;
            this.col_0_20Lakh.HeaderText = "0.00~2000000.00";
            this.col_0_20Lakh.Name = "col_0_20Lakh";
            this.col_0_20Lakh.ReadOnly = true;
            // 
            // col_20Lakh_50Lakh
            // 
            this.col_20Lakh_50Lakh.DataPropertyName = "2000001.00~5000000.00";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_20Lakh_50Lakh.DefaultCellStyle = dataGridViewCellStyle10;
            this.col_20Lakh_50Lakh.HeaderText = "2000000.00~5000000.00";
            this.col_20Lakh_50Lakh.Name = "col_20Lakh_50Lakh";
            this.col_20Lakh_50Lakh.ReadOnly = true;
            // 
            // col_50Lakh_100Lakh
            // 
            this.col_50Lakh_100Lakh.DataPropertyName = "5000001.00~10000000.00";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_50Lakh_100Lakh.DefaultCellStyle = dataGridViewCellStyle11;
            this.col_50Lakh_100Lakh.HeaderText = "5000000.00~10000000.00";
            this.col_50Lakh_100Lakh.Name = "col_50Lakh_100Lakh";
            this.col_50Lakh_100Lakh.ReadOnly = true;
            // 
            // col_100Lakh_200Lakh
            // 
            this.col_100Lakh_200Lakh.DataPropertyName = "10000001.00~20000000.00";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_100Lakh_200Lakh.DefaultCellStyle = dataGridViewCellStyle12;
            this.col_100Lakh_200Lakh.HeaderText = "10000000.00~20000000.00";
            this.col_100Lakh_200Lakh.Name = "col_100Lakh_200Lakh";
            this.col_100Lakh_200Lakh.ReadOnly = true;
            // 
            // col_200Lakh_300Lakh
            // 
            this.col_200Lakh_300Lakh.DataPropertyName = "20000001.00~30000000.00";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_200Lakh_300Lakh.DefaultCellStyle = dataGridViewCellStyle13;
            this.col_200Lakh_300Lakh.HeaderText = "20000000.00~30000000.00";
            this.col_200Lakh_300Lakh.Name = "col_200Lakh_300Lakh";
            this.col_200Lakh_300Lakh.ReadOnly = true;
            // 
            // col_300Lakh_above
            // 
            this.col_300Lakh_above.DataPropertyName = "30000001.00~";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_300Lakh_above.DefaultCellStyle = dataGridViewCellStyle14;
            this.col_300Lakh_above.HeaderText = "3000000.00~Above";
            this.col_300Lakh_above.Name = "col_300Lakh_above";
            this.col_300Lakh_above.ReadOnly = true;
            // 
            // col_total_tax
            // 
            this.col_total_tax.DataPropertyName = "TotalTax";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_total_tax.DefaultCellStyle = dataGridViewCellStyle15;
            this.col_total_tax.HeaderText = "Total Tax";
            this.col_total_tax.Name = "col_total_tax";
            this.col_total_tax.ReadOnly = true;
            // 
            // col_monthly_tax
            // 
            this.col_monthly_tax.DataPropertyName = "MonthlyTax";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_monthly_tax.DefaultCellStyle = dataGridViewCellStyle16;
            this.col_monthly_tax.HeaderText = "Monthly Tax";
            this.col_monthly_tax.Name = "col_monthly_tax";
            this.col_monthly_tax.ReadOnly = true;
            // 
            // frmTaxableMonthlyPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 681);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmTaxableMonthlyPayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TaxableMonthlyPayment";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmTaxableMonthlyPayment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonthly)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkEmployee;
        private System.Windows.Forms.ComboBox cmbEmployee;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.DateTimePicker dtpMonth;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.DataGridView dgvMonthly;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ProgressBar pbMonthlyCalculate;
        private System.ComponentModel.BackgroundWorker bgwMonthlyCalculate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_empid;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_empname;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_monthly_salary;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_annual_salary;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_personal_relief;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_parents_relief;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_spouse_relief;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_child_relief;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_other;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_taxable_income;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_0_20Lakh;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_20Lakh_50Lakh;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_50Lakh_100Lakh;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_100Lakh_200Lakh;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_200Lakh_300Lakh;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_300Lakh_above;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_total_tax;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_monthly_tax;
    }
}