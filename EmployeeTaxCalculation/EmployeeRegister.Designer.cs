﻿namespace EmployeeTaxCalculation
{
    partial class frmEmployeeRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblMonthlyNetSalary = new System.Windows.Forms.Label();
            this.lblNoOfParents = new System.Windows.Forms.Label();
            this.lblNoOfSpouse = new System.Windows.Forms.Label();
            this.lblNoOfChildren = new System.Windows.Forms.Label();
            this.txtEmployeeName = new System.Windows.Forms.TextBox();
            this.txtMonthlyNetSalary = new System.Windows.Forms.TextBox();
            this.txtNoOfParents = new System.Windows.Forms.TextBox();
            this.txtSpouse = new System.Windows.Forms.TextBox();
            this.txtNoOfChildren = new System.Windows.Forms.TextBox();
            this.btnEmployeeRegister = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblNRC = new System.Windows.Forms.Label();
            this.txtNRC = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.Location = new System.Drawing.Point(23, 38);
            this.lblEmployeeName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(109, 24);
            this.lblEmployeeName.TabIndex = 0;
            this.lblEmployeeName.Text = "Employee Name";
            // 
            // lblMonthlyNetSalary
            // 
            this.lblMonthlyNetSalary.AutoSize = true;
            this.lblMonthlyNetSalary.Location = new System.Drawing.Point(23, 115);
            this.lblMonthlyNetSalary.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonthlyNetSalary.Name = "lblMonthlyNetSalary";
            this.lblMonthlyNetSalary.Size = new System.Drawing.Size(126, 24);
            this.lblMonthlyNetSalary.TabIndex = 1;
            this.lblMonthlyNetSalary.Text = "Monthly Net Salary";
            // 
            // lblNoOfParents
            // 
            this.lblNoOfParents.AutoSize = true;
            this.lblNoOfParents.Location = new System.Drawing.Point(23, 155);
            this.lblNoOfParents.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNoOfParents.Name = "lblNoOfParents";
            this.lblNoOfParents.Size = new System.Drawing.Size(126, 24);
            this.lblNoOfParents.TabIndex = 2;
            this.lblNoOfParents.Text = "Number Of Parents";
            // 
            // lblNoOfSpouse
            // 
            this.lblNoOfSpouse.AutoSize = true;
            this.lblNoOfSpouse.Location = new System.Drawing.Point(23, 195);
            this.lblNoOfSpouse.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNoOfSpouse.Name = "lblNoOfSpouse";
            this.lblNoOfSpouse.Size = new System.Drawing.Size(54, 24);
            this.lblNoOfSpouse.TabIndex = 3;
            this.lblNoOfSpouse.Text = "Spouse";
            // 
            // lblNoOfChildren
            // 
            this.lblNoOfChildren.AutoSize = true;
            this.lblNoOfChildren.Location = new System.Drawing.Point(23, 235);
            this.lblNoOfChildren.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNoOfChildren.Name = "lblNoOfChildren";
            this.lblNoOfChildren.Size = new System.Drawing.Size(131, 24);
            this.lblNoOfChildren.TabIndex = 4;
            this.lblNoOfChildren.Text = "Number Of Children";
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Location = new System.Drawing.Point(162, 35);
            this.txtEmployeeName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Size = new System.Drawing.Size(150, 30);
            this.txtEmployeeName.TabIndex = 5;
            // 
            // txtMonthlyNetSalary
            // 
            this.txtMonthlyNetSalary.Location = new System.Drawing.Point(162, 112);
            this.txtMonthlyNetSalary.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMonthlyNetSalary.Name = "txtMonthlyNetSalary";
            this.txtMonthlyNetSalary.Size = new System.Drawing.Size(150, 30);
            this.txtMonthlyNetSalary.TabIndex = 6;
            this.txtMonthlyNetSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMonthlyNetSalary_KeyPress);
            // 
            // txtNoOfParents
            // 
            this.txtNoOfParents.Location = new System.Drawing.Point(162, 152);
            this.txtNoOfParents.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNoOfParents.Name = "txtNoOfParents";
            this.txtNoOfParents.Size = new System.Drawing.Size(150, 30);
            this.txtNoOfParents.TabIndex = 7;
            this.txtNoOfParents.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoOfParents_KeyPress);
            // 
            // txtSpouse
            // 
            this.txtSpouse.Location = new System.Drawing.Point(162, 192);
            this.txtSpouse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSpouse.Name = "txtSpouse";
            this.txtSpouse.Size = new System.Drawing.Size(150, 30);
            this.txtSpouse.TabIndex = 8;
            this.txtSpouse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSpouse_KeyPress);
            // 
            // txtNoOfChildren
            // 
            this.txtNoOfChildren.Location = new System.Drawing.Point(162, 232);
            this.txtNoOfChildren.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNoOfChildren.Name = "txtNoOfChildren";
            this.txtNoOfChildren.Size = new System.Drawing.Size(150, 30);
            this.txtNoOfChildren.TabIndex = 9;
            // 
            // btnEmployeeRegister
            // 
            this.btnEmployeeRegister.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEmployeeRegister.Location = new System.Drawing.Point(117, 272);
            this.btnEmployeeRegister.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnEmployeeRegister.Name = "btnEmployeeRegister";
            this.btnEmployeeRegister.Size = new System.Drawing.Size(100, 50);
            this.btnEmployeeRegister.TabIndex = 10;
            this.btnEmployeeRegister.Text = "Register";
            this.btnEmployeeRegister.UseVisualStyleBackColor = true;
            this.btnEmployeeRegister.Click += new System.EventHandler(this.btnEmployeeRegister_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtNRC);
            this.panel1.Controls.Add(this.lblNRC);
            this.panel1.Controls.Add(this.lblNoOfParents);
            this.panel1.Controls.Add(this.btnEmployeeRegister);
            this.panel1.Controls.Add(this.lblEmployeeName);
            this.panel1.Controls.Add(this.txtNoOfChildren);
            this.panel1.Controls.Add(this.lblMonthlyNetSalary);
            this.panel1.Controls.Add(this.txtSpouse);
            this.panel1.Controls.Add(this.lblNoOfSpouse);
            this.panel1.Controls.Add(this.txtNoOfParents);
            this.panel1.Controls.Add(this.lblNoOfChildren);
            this.panel1.Controls.Add(this.txtMonthlyNetSalary);
            this.panel1.Controls.Add(this.txtEmployeeName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(329, 343);
            this.panel1.TabIndex = 11;
            // 
            // lblNRC
            // 
            this.lblNRC.AutoSize = true;
            this.lblNRC.Location = new System.Drawing.Point(23, 76);
            this.lblNRC.Name = "lblNRC";
            this.lblNRC.Size = new System.Drawing.Size(59, 24);
            this.lblNRC.TabIndex = 11;
            this.lblNRC.Text = "NRC No.";
            // 
            // txtNRC
            // 
            this.txtNRC.Location = new System.Drawing.Point(162, 74);
            this.txtNRC.Name = "txtNRC";
            this.txtNRC.Size = new System.Drawing.Size(150, 30);
            this.txtNRC.TabIndex = 12;
            this.txtNRC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNRC_KeyPress);
            // 
            // frmEmployeeRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 343);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmEmployeeRegister";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Register";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblMonthlyNetSalary;
        private System.Windows.Forms.Label lblNoOfParents;
        private System.Windows.Forms.Label lblNoOfSpouse;
        private System.Windows.Forms.Label lblNoOfChildren;
        private System.Windows.Forms.TextBox txtEmployeeName;
        private System.Windows.Forms.TextBox txtMonthlyNetSalary;
        private System.Windows.Forms.TextBox txtNoOfParents;
        private System.Windows.Forms.TextBox txtSpouse;
        private System.Windows.Forms.TextBox txtNoOfChildren;
        private System.Windows.Forms.Button btnEmployeeRegister;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtNRC;
        private System.Windows.Forms.Label lblNRC;
    }
}

