﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmMenu : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        public frmMenu()
        {
            InitializeComponent();
        }

        private void btnEmpList_Click(object sender, EventArgs e)
        {
            frmEmployeeList frmEmpList = new frmEmployeeList();
            frmEmpList.Show();
        }

        private void btnEmpRegister_Click(object sender, EventArgs e)
        {
            frmEmployeeRegister frmEmpReg = new frmEmployeeRegister();
            frmEmpReg.Show();
        }

        private void btnEmpUpdate_Click(object sender, EventArgs e)
        {
            frmEmployeeUpdate frmEmpUpdate = new frmEmployeeUpdate();
            frmEmpUpdate.Show();
        }

       

        private void btnReliefType_Click(object sender, EventArgs e)
        {
            frmReliefType frmRefType = new frmReliefType();
            frmRefType.Show();
        }

        private void btnTaxRate_Click(object sender, EventArgs e)
        {
            frmTaxRate frmRate = new frmTaxRate();
            frmRate.Show();
        }

        private void btnMonthlyTax_Click(object sender, EventArgs e)
        {
            frmTaxableMonthlyPayment frmMonthly = new frmTaxableMonthlyPayment();
            frmMonthly.Show();
        }

        private void btnMonthlyTaxList_Click(object sender, EventArgs e)
        {
            frmTaxableMonthlyPaymentList frmMonthlyList = new frmTaxableMonthlyPaymentList();
            frmMonthlyList.Show();
        }

        private void btnYearlyTax_Click(object sender, EventArgs e)
        {
            frmAdjustmentList frmAdjList = new frmAdjustmentList();
            frmAdjList.Show();
        }

        private void btnDetailReport_Click(object sender, EventArgs e)
        {
            frmTaxDetailReport frmDetail = new frmTaxDetailReport();
            frmDetail.Show();
        }

        private void btnYearlyReport_Click(object sender, EventArgs e)
        {
            frmTaxYearlySummaryReport frmYearly = new frmTaxYearlySummaryReport();
            frmYearly.Show();
        }

        private void btnEducation_Click(object sender, EventArgs e)
        {
            frmEducation frmEdu = new frmEducation();
            frmEdu.Show();
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {
           
        }

        private void btnCurrency_Click(object sender, EventArgs e)
        {
            frmCurrencyList frmCury = new frmCurrencyList();
            frmCury.Show();
        }

        private void btnEmpBankReport_Click(object sender, EventArgs e)
        {
            frmEmpBankAccHistoryReport frmEmpBank = new frmEmpBankAccHistoryReport();
            frmEmpBank.Show();
        }
    }
}
