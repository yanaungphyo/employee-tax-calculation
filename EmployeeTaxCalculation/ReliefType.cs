﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmReliefType : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmdReliefType;
        SqlDataAdapter daReliefType;
        decimal d;
        
        public frmReliefType()
        {
            InitializeComponent();
            if (!btnSaveRefType.Enabled)
            {
                btnSaveRefType.Enabled = true;
            }
            if (btnUpdateRefType.Enabled)
            {
                btnUpdateRefType.Enabled = false;
            }
            if (string.IsNullOrWhiteSpace(cmbRelative.Text))
            {
                cmbRelative.Text = "--Please select--";
            }
            dgvReliefType.AutoGenerateColumns = false;            
            DisplayData();
        }
    
        bool IsFixed;
        
        private void btnSaveRefType_Click(object sender, EventArgs e)
        {
            if(con.State != ConnectionState.Open)
            con.Open();
            
            

            if (txtReliefName.Text == "")
            {
                MessageBox.Show("Please enter Relief Name!");
                return;
            }
            if (!decimal.TryParse(txtRefAmt.Text, out d) || Decimal.Parse(txtRefAmt.Text) <= 0)
            {
                //invalid
                MessageBox.Show("Please enter a valid number");
                return;
            }

            
                if (rdoFixed.Checked == true)
                {
                    IsFixed = true;
                }
                else if (rdoPercentage.Checked == true)
                {
                    IsFixed = false;
                    if (Decimal.Parse(txtRefAmt.Text) < 0 || Decimal.Parse(txtRefAmt.Text) > 100)
                    {
                        MessageBox.Show("Please enter between 0% and 100%");
                        return;
                    }

                }
                if (cmbRelative.SelectedItem == null)
                {
                    MessageBox.Show("Please select Relative Type");
                    return;
                }
                cmdReliefType = new SqlCommand(@"INSERT INTO Relief_Type (relief_name, IsFixed, relief_amount, relative_type_id, is_deleted, created_date, updated_date) values (@relief_name, @IsFixed, @relief_amount, @relative_type_id, @is_deleted, @created_date, @updated_date)", con);

                cmdReliefType.Parameters.AddWithValue("@relief_name", txtReliefName.Text);
                cmdReliefType.Parameters.AddWithValue("@IsFixed", IsFixed);
                cmdReliefType.Parameters.AddWithValue("@relief_amount", Decimal.Parse(txtRefAmt.Text));
                cmdReliefType.Parameters.AddWithValue("@relative_type_id", cmbRelative.SelectedIndex);
                cmdReliefType.Parameters.AddWithValue("@is_deleted", Boolean.FalseString);
                cmdReliefType.Parameters.AddWithValue("@created_date", DateTime.Now);
                cmdReliefType.Parameters.AddWithValue("@updated_date", DateTime.Now);
                cmdReliefType.ExecuteNonQuery();
                MessageBox.Show("Successful");
                con.Close();

                DisplayData();
                Clear();
           
            
        }

        private void btnUpdateRefType_Click(object sender, EventArgs e)
        {
            
            if (con.State != ConnectionState.Open)
                con.Open();
            if (txtReliefName.Text == "")
            {
                MessageBox.Show("Please enter Relief Name!");
                return;
            }
            if (!decimal.TryParse(txtRefAmt.Text, out d) || Decimal.Parse(txtRefAmt.Text) <= 0)
            {
                //invalid
                MessageBox.Show("Please enter a valid number");
                return;
            }


            if (rdoFixed.Checked == true)
            {
                IsFixed = true;
            }
            else if (rdoPercentage.Checked == true)
            {
                IsFixed = false;
                if (Decimal.Parse(txtRefAmt.Text) < 0 || Decimal.Parse(txtRefAmt.Text) > 100)
                {
                    MessageBox.Show("Please enter between 0% and 100%");
                    return;
                }

            }
            if (cmbRelative.SelectedItem == null)
            {
                MessageBox.Show("Please select Relative Type");
                return;
            }
                int ReliefTypeID = int.Parse(dgvReliefType.SelectedRows[0].Cells["Col_ReliefType_id"].Value.ToString());
                cmdReliefType = new SqlCommand(@"update Relief_Type set relief_name=@relief_name,  isFixed=@isFixed,
                relief_amount=@relief_amount, relative_type_id=@relative_type_id, updated_date=@updated_date where id=@id", con);
                cmdReliefType.Parameters.AddWithValue("@id", ReliefTypeID);
                cmdReliefType.Parameters.AddWithValue("@relief_name", txtReliefName.Text);
                cmdReliefType.Parameters.AddWithValue("@isFixed", IsFixed);
                cmdReliefType.Parameters.AddWithValue("@relief_amount", txtRefAmt.Text);
                cmdReliefType.Parameters.AddWithValue("@relative_type_id", cmbRelative.SelectedIndex);
                cmdReliefType.Parameters.AddWithValue("@updated_date", DateTime.Now);

                cmdReliefType.ExecuteNonQuery();
                MessageBox.Show("Updated Successfully");
                con.Close();
                if (!btnSaveRefType.Enabled)
                {
                    btnSaveRefType.Enabled = true;
                }
                btnUpdateRefType.Enabled = false;
                DisplayData();
                Clear();

            
        }

        private void DisplayData()
        {
            con.Open();
            DataTable dt = new DataTable();
            daReliefType = new SqlDataAdapter(@"select id,relief_name,(case when isFixed = 0 then 'Percentage' else 'Fixed' end) As status,
relief_amount,(case when relative_type_id=0 then 'Self' when relative_type_id=1 then 'Parents' 
when relative_type_id=2 then 'Spouse' when relative_type_id=3 then 'Children' end) As Relative_Type from Relief_Type where is_deleted=0", con);
            daReliefType.Fill(dt);
            dgvReliefType.DataSource = dt;

            con.Close();
        }

        void Clear()
        {
            txtReliefName.Text = "";
            txtRefAmt.Text = "";
            cmbRelative.Text = "";
            
        }

        private void dgvReliefType_DoubleClick(object sender, EventArgs e)
        {
            btnSaveRefType.Enabled = false;
            if (!btnUpdateRefType.Enabled)
            {
                btnUpdateRefType.Enabled = true;
            }
            txtReliefName.Text = dgvReliefType.CurrentRow.Cells[2].Value.ToString();

            //if (dgvReliefType.CurrentRow.Cells["Col_ReliefType_id"].Value != null)
            //    this.ReliefTypeID = int.Parse(dgvReliefType.CurrentRow.Cells["Col_ReliefType_id"].Value.ToString());

            if (this.dgvReliefType.CurrentRow.Cells[3].Value.Equals("Fixed"))
                rdoFixed.Checked = true;
            else
                rdoFixed.Checked = false;

            if (this.dgvReliefType.CurrentRow.Cells[3].Value.Equals("Percentage"))
                rdoPercentage.Checked = true;
            else
                rdoPercentage.Checked = false;

            txtRefAmt.Text = dgvReliefType.CurrentRow.Cells[4].Value.ToString();

            cmbRelative.Text=dgvReliefType.CurrentRow.Cells[5].Value.ToString();
        }

        private void dgvReliefType_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dgvReliefType.Rows[e.RowIndex].Cells["Col_No"].Value = e.RowIndex + 1;
        }

        private void btnDeleteRefType_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)
            con.Open();
            try
            {
                DialogResult result = MessageBox.Show("Are you sure you want to delete?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    int reliefID;

                    reliefID = int.Parse(dgvReliefType.SelectedRows[0].Cells["Col_ReliefType_id"].Value.ToString());
                    cmdReliefType = new SqlCommand("UPDATE Relief_Type SET  is_deleted=1 where id=@id", con);
                    cmdReliefType.Parameters.AddWithValue("@id", reliefID);
                    cmdReliefType.ExecuteNonQuery();

                    MessageBox.Show("Deleted Successfully!");

                }
                con.Close();
                DisplayData();
            }
            catch (Exception ex)
            { MessageBox.Show("Please select data"); }
        }

        //private void txtRefAmt_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    char ch = e.KeyChar;
        //    if (!Char.IsDigit(ch) && ch != 8 && ch!=46)
        //    {
        //        e.Handled = true; 
        //    }
        //}

        

             

    }
}
