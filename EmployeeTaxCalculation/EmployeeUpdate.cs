﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmEmployeeUpdate : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmdEmployeeUpdate;      
        
        int EmployeeID;
        decimal d;
        int i;

        public frmEmployeeUpdate(/*int EmpID*/)
        {
            InitializeComponent();
           // this.EmployeeID = EmpID;
        }
        public frmEmployeeUpdate(int EmpID,string EmpName, string NRC, string EmpMonthlySalary, string EmpParents, string EmpSoupse, string EmpChildren)
        {
            InitializeComponent();
            this.EmployeeID = EmpID;
            txtEmployeeName.Text = EmpName;
            txtNRC.Text = NRC;
            txtMonthlyNetSalary.Text = EmpMonthlySalary;
            txtNoOfParents.Text = EmpParents;
            txtSpouse.Text = EmpSoupse;
            txtNoOfChildren.Text = EmpChildren;
        }

        private void btnEmployeeUpdate_Click(object sender, EventArgs e)
        {
                
                
            if(con.State!=ConnectionState.Open)
                con.Open();            

            if (txtEmployeeName.Text == "")
            {
                MessageBox.Show("Please enter Name!");
                return;
            }
            if (txtNRC.Text == "")
            {
                MessageBox.Show("Please enter NRC No.");
            }
            if (!decimal.TryParse(txtMonthlyNetSalary.Text, out d) || Decimal.Parse(txtMonthlyNetSalary.Text) < 0)
            {
                MessageBox.Show("Please enter Monthly Net Salary");
                return;
            }
            if (!int.TryParse(txtNoOfParents.Text, out i) || Int32.Parse(txtNoOfParents.Text) < 0 || Int32.Parse(txtNoOfParents.Text) > 2)
            {
                MessageBox.Show("Please enter Parent Numbers");
                return;
            }
            if (!int.TryParse(txtSpouse.Text, out i) || Convert.ToInt32(txtSpouse.Text) < 0 || Convert.ToInt32(txtSpouse.Text) > 1)
            {
                MessageBox.Show("Please enter Spouse (eg..0,1)");
                return;
            }
            if (!int.TryParse(txtNoOfChildren.Text, out i) || Int32.Parse(txtNoOfChildren.Text) < 0)
            {
                MessageBox.Show("Please enter Children (eg..0,1,2,etc...)");
                return;
            }
           
                string queryEmployeeUpdate = "UPDATE Employee SET employee_name=@employee_name, NRC_No=@NRC_No, monthly_net_salary=@monthly_net_salary, no_of_parents=@no_of_parents, no_of_spouse=@no_of_spouse, no_of_children=@no_of_children, updated_date=@updated_date WHERE id=@id";
                cmdEmployeeUpdate = new SqlCommand(queryEmployeeUpdate, con);
                cmdEmployeeUpdate.Parameters.AddWithValue("@id", this.EmployeeID);
                cmdEmployeeUpdate.Parameters.AddWithValue("@employee_name", txtEmployeeName.Text);
                cmdEmployeeUpdate.Parameters.AddWithValue("@NRC_No", txtNRC.Text);
                cmdEmployeeUpdate.Parameters.AddWithValue("@monthly_net_salary", Decimal.Parse(txtMonthlyNetSalary.Text));
                cmdEmployeeUpdate.Parameters.AddWithValue("@no_of_parents", Int32.Parse(txtNoOfParents.Text));
                cmdEmployeeUpdate.Parameters.AddWithValue("@no_of_spouse", Int32.Parse(txtSpouse.Text));
                cmdEmployeeUpdate.Parameters.AddWithValue("@no_of_children", Int32.Parse(txtNoOfChildren.Text));
                cmdEmployeeUpdate.Parameters.AddWithValue("@updated_date", DateTime.Now);
                cmdEmployeeUpdate.ExecuteNonQuery();
                MessageBox.Show("Updated Successfully");                
                con.Close();
                this.Close();
            
            }

        private void txtMonthlyNetSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void txtNRC_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled=(e.KeyChar==(char)Keys.Space);
        }
                
 
        }      
      

    
}
