﻿namespace EmployeeTaxCalculation
{
    partial class frmReliefType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblReliefName = new System.Windows.Forms.Label();
            this.lblisFixed = new System.Windows.Forms.Label();
            this.txtReliefName = new System.Windows.Forms.TextBox();
            this.rdoFixed = new System.Windows.Forms.RadioButton();
            this.rdoPercentage = new System.Windows.Forms.RadioButton();
            this.btnSaveRefType = new System.Windows.Forms.Button();
            this.btnUpdateRefType = new System.Windows.Forms.Button();
            this.dgvReliefType = new System.Windows.Forms.DataGridView();
            this.Col_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_ReliefType_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Relief_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_isFixed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_relief_amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_relative = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDeleteRefType = new System.Windows.Forms.Button();
            this.lblReliefAmount = new System.Windows.Forms.Label();
            this.txtRefAmt = new System.Windows.Forms.TextBox();
            this.lblRelative = new System.Windows.Forms.Label();
            this.cmbRelative = new System.Windows.Forms.ComboBox();
            this.pnlRelief = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReliefType)).BeginInit();
            this.pnlRelief.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblReliefName
            // 
            this.lblReliefName.AutoSize = true;
            this.lblReliefName.Location = new System.Drawing.Point(3, 14);
            this.lblReliefName.Name = "lblReliefName";
            this.lblReliefName.Size = new System.Drawing.Size(82, 24);
            this.lblReliefName.TabIndex = 0;
            this.lblReliefName.Text = "Relief Name";
            // 
            // lblisFixed
            // 
            this.lblisFixed.AutoSize = true;
            this.lblisFixed.Location = new System.Drawing.Point(3, 55);
            this.lblisFixed.Name = "lblisFixed";
            this.lblisFixed.Size = new System.Drawing.Size(55, 24);
            this.lblisFixed.TabIndex = 1;
            this.lblisFixed.Text = "Is Fixed";
            // 
            // txtReliefName
            // 
            this.txtReliefName.Location = new System.Drawing.Point(126, 11);
            this.txtReliefName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtReliefName.Name = "txtReliefName";
            this.txtReliefName.Size = new System.Drawing.Size(116, 30);
            this.txtReliefName.TabIndex = 2;
            // 
            // rdoFixed
            // 
            this.rdoFixed.AutoSize = true;
            this.rdoFixed.Checked = true;
            this.rdoFixed.Location = new System.Drawing.Point(126, 52);
            this.rdoFixed.Name = "rdoFixed";
            this.rdoFixed.Size = new System.Drawing.Size(59, 28);
            this.rdoFixed.TabIndex = 3;
            this.rdoFixed.TabStop = true;
            this.rdoFixed.Text = "Fixed";
            this.rdoFixed.UseVisualStyleBackColor = true;
            // 
            // rdoPercentage
            // 
            this.rdoPercentage.AutoSize = true;
            this.rdoPercentage.Location = new System.Drawing.Point(192, 52);
            this.rdoPercentage.Name = "rdoPercentage";
            this.rdoPercentage.Size = new System.Drawing.Size(95, 28);
            this.rdoPercentage.TabIndex = 4;
            this.rdoPercentage.Text = "Percentage";
            this.rdoPercentage.UseVisualStyleBackColor = true;
            // 
            // btnSaveRefType
            // 
            this.btnSaveRefType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveRefType.Location = new System.Drawing.Point(7, 183);
            this.btnSaveRefType.Name = "btnSaveRefType";
            this.btnSaveRefType.Size = new System.Drawing.Size(100, 30);
            this.btnSaveRefType.TabIndex = 5;
            this.btnSaveRefType.Text = "Save";
            this.btnSaveRefType.UseVisualStyleBackColor = true;
            this.btnSaveRefType.Click += new System.EventHandler(this.btnSaveRefType_Click);
            // 
            // btnUpdateRefType
            // 
            this.btnUpdateRefType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdateRefType.Location = new System.Drawing.Point(113, 183);
            this.btnUpdateRefType.Name = "btnUpdateRefType";
            this.btnUpdateRefType.Size = new System.Drawing.Size(100, 30);
            this.btnUpdateRefType.TabIndex = 6;
            this.btnUpdateRefType.Text = "Update";
            this.btnUpdateRefType.UseVisualStyleBackColor = true;
            this.btnUpdateRefType.Click += new System.EventHandler(this.btnUpdateRefType_Click);
            // 
            // dgvReliefType
            // 
            this.dgvReliefType.AllowUserToAddRows = false;
            this.dgvReliefType.AllowUserToDeleteRows = false;
            this.dgvReliefType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReliefType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_No,
            this.Col_ReliefType_id,
            this.Col_Relief_Name,
            this.Col_isFixed,
            this.col_relief_amount,
            this.col_relative});
            this.dgvReliefType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvReliefType.Location = new System.Drawing.Point(0, 0);
            this.dgvReliefType.MultiSelect = false;
            this.dgvReliefType.Name = "dgvReliefType";
            this.dgvReliefType.ReadOnly = true;
            this.dgvReliefType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReliefType.Size = new System.Drawing.Size(543, 263);
            this.dgvReliefType.TabIndex = 7;
            this.dgvReliefType.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvReliefType_RowPostPaint);
            this.dgvReliefType.DoubleClick += new System.EventHandler(this.dgvReliefType_DoubleClick);
            // 
            // Col_No
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Col_No.DefaultCellStyle = dataGridViewCellStyle1;
            this.Col_No.HeaderText = "No";
            this.Col_No.Name = "Col_No";
            this.Col_No.ReadOnly = true;
            // 
            // Col_ReliefType_id
            // 
            this.Col_ReliefType_id.DataPropertyName = "id";
            this.Col_ReliefType_id.HeaderText = "id";
            this.Col_ReliefType_id.Name = "Col_ReliefType_id";
            this.Col_ReliefType_id.ReadOnly = true;
            this.Col_ReliefType_id.Visible = false;
            // 
            // Col_Relief_Name
            // 
            this.Col_Relief_Name.DataPropertyName = "relief_name";
            this.Col_Relief_Name.HeaderText = "Relief Name";
            this.Col_Relief_Name.Name = "Col_Relief_Name";
            this.Col_Relief_Name.ReadOnly = true;
            // 
            // Col_isFixed
            // 
            this.Col_isFixed.DataPropertyName = "status";
            this.Col_isFixed.HeaderText = "isFixed";
            this.Col_isFixed.Name = "Col_isFixed";
            this.Col_isFixed.ReadOnly = true;
            // 
            // col_relief_amount
            // 
            this.col_relief_amount.DataPropertyName = "relief_amount";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_relief_amount.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_relief_amount.HeaderText = "Relief Amount";
            this.col_relief_amount.Name = "col_relief_amount";
            this.col_relief_amount.ReadOnly = true;
            // 
            // col_relative
            // 
            this.col_relative.DataPropertyName = "Relative_Type";
            this.col_relative.HeaderText = "Relative Type";
            this.col_relative.Name = "col_relative";
            this.col_relative.ReadOnly = true;
            // 
            // btnDeleteRefType
            // 
            this.btnDeleteRefType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeleteRefType.Location = new System.Drawing.Point(219, 183);
            this.btnDeleteRefType.Name = "btnDeleteRefType";
            this.btnDeleteRefType.Size = new System.Drawing.Size(100, 30);
            this.btnDeleteRefType.TabIndex = 8;
            this.btnDeleteRefType.Text = "Delete";
            this.btnDeleteRefType.UseVisualStyleBackColor = true;
            this.btnDeleteRefType.Click += new System.EventHandler(this.btnDeleteRefType_Click);
            // 
            // lblReliefAmount
            // 
            this.lblReliefAmount.AutoSize = true;
            this.lblReliefAmount.Location = new System.Drawing.Point(3, 94);
            this.lblReliefAmount.Name = "lblReliefAmount";
            this.lblReliefAmount.Size = new System.Drawing.Size(96, 24);
            this.lblReliefAmount.TabIndex = 9;
            this.lblReliefAmount.Text = "Relief Amount";
            // 
            // txtRefAmt
            // 
            this.txtRefAmt.Location = new System.Drawing.Point(126, 91);
            this.txtRefAmt.Name = "txtRefAmt";
            this.txtRefAmt.Size = new System.Drawing.Size(100, 30);
            this.txtRefAmt.TabIndex = 10;
            // 
            // lblRelative
            // 
            this.lblRelative.AutoSize = true;
            this.lblRelative.Location = new System.Drawing.Point(3, 138);
            this.lblRelative.Name = "lblRelative";
            this.lblRelative.Size = new System.Drawing.Size(92, 24);
            this.lblRelative.TabIndex = 11;
            this.lblRelative.Text = "Relative Type";
            // 
            // cmbRelative
            // 
            this.cmbRelative.FormattingEnabled = true;
            this.cmbRelative.Items.AddRange(new object[] {
            "Self",
            "Parents",
            "Spouse",
            "Children"});
            this.cmbRelative.Location = new System.Drawing.Point(126, 135);
            this.cmbRelative.Name = "cmbRelative";
            this.cmbRelative.Size = new System.Drawing.Size(150, 30);
            this.cmbRelative.TabIndex = 12;
            // 
            // pnlRelief
            // 
            this.pnlRelief.Controls.Add(this.rdoFixed);
            this.pnlRelief.Controls.Add(this.btnDeleteRefType);
            this.pnlRelief.Controls.Add(this.cmbRelative);
            this.pnlRelief.Controls.Add(this.lblReliefName);
            this.pnlRelief.Controls.Add(this.btnUpdateRefType);
            this.pnlRelief.Controls.Add(this.lblRelative);
            this.pnlRelief.Controls.Add(this.btnSaveRefType);
            this.pnlRelief.Controls.Add(this.lblisFixed);
            this.pnlRelief.Controls.Add(this.txtRefAmt);
            this.pnlRelief.Controls.Add(this.txtReliefName);
            this.pnlRelief.Controls.Add(this.lblReliefAmount);
            this.pnlRelief.Controls.Add(this.rdoPercentage);
            this.pnlRelief.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRelief.Location = new System.Drawing.Point(0, 0);
            this.pnlRelief.Name = "pnlRelief";
            this.pnlRelief.Size = new System.Drawing.Size(543, 216);
            this.pnlRelief.TabIndex = 13;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvReliefType);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 216);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(543, 263);
            this.panel1.TabIndex = 14;
            // 
            // frmReliefType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 479);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlRelief);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmReliefType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReliefType";
            ((System.ComponentModel.ISupportInitialize)(this.dgvReliefType)).EndInit();
            this.pnlRelief.ResumeLayout(false);
            this.pnlRelief.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblReliefName;
        private System.Windows.Forms.Label lblisFixed;
        private System.Windows.Forms.TextBox txtReliefName;
        private System.Windows.Forms.RadioButton rdoFixed;
        private System.Windows.Forms.RadioButton rdoPercentage;
        private System.Windows.Forms.Button btnSaveRefType;
        private System.Windows.Forms.Button btnUpdateRefType;
        private System.Windows.Forms.DataGridView dgvReliefType;
        private System.Windows.Forms.Button btnDeleteRefType;
        private System.Windows.Forms.Label lblReliefAmount;
        private System.Windows.Forms.TextBox txtRefAmt;
        private System.Windows.Forms.Label lblRelative;
        private System.Windows.Forms.ComboBox cmbRelative;
        private System.Windows.Forms.Panel pnlRelief;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_No;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_ReliefType_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Relief_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_isFixed;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_relief_amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_relative;
    }
}