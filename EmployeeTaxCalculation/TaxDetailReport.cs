﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace EmployeeTaxCalculation
{
    public partial class frmTaxDetailReport : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;

        public frmTaxDetailReport()
        {
            InitializeComponent();
        }

        public frmTaxDetailReport(DateTime sDate,DateTime eDate)
        {
            InitializeComponent();
        }

        private void TaxDetailReport_Load(object sender, EventArgs e)
        {
            dtpMonthFrom.Value = DateTime.Today;
            dtpMonthTo.Value = DateTime.Today;
            string querySelect = "select id,employee_name from Employee where is_deleted=0";
            cmd = new SqlCommand(querySelect, con);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            
            cmbEmpName.DisplayMember = "employee_name";
            cmbEmpName.ValueMember = "id";
            cmbEmpName.DataSource = dt;
            this.cmbEmpName.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cmbEmpName.AutoCompleteSource = AutoCompleteSource.ListItems;
            con.Close();

            this.rpvTaxDetail.RefreshReport();
            this.rpvTaxDetail.RefreshReport();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)
            con.Open();

            

                string sDate =dtpMonthFrom.Value.ToString("MMM-yyyy");
                string eDate = dtpMonthTo.Value.ToString("MMM-yyyy");
                
                string query = @"select * from vw_detail_report ";
                if (chkEmpName.Checked == true & chkMonth.Checked == true)
                {
                    if (cmbEmpName.SelectedItem == null)
                    {
                        MessageBox.Show("Please select Employee Name");
                        return;
                    }
                    if (dtpMonthTo.Value >= dtpMonthFrom.Value)
                    {
                        query += @"where employee_name='" + cmbEmpName.Text + "'" + " and (taxable_month between " + dtpMonthFrom.Value.Month + " and " + dtpMonthTo.Value.Month + ") and (taxable_year between " + dtpMonthFrom.Value.Year + " and " + dtpMonthTo.Value.Year + ") ";
                    }
                    else MessageBox.Show("FromMonth is greater than ToMonth");

                }
                else if (chkMonth.Checked == true)
                {
                    if (dtpMonthTo.Value >= dtpMonthFrom.Value)
                    {
                    query += @"where (taxable_month between " + dtpMonthFrom.Value.Month + " and " + dtpMonthTo.Value.Month + ") and (taxable_year between " + dtpMonthFrom.Value.Year + " and " + dtpMonthTo.Value.Year + ") ";
                    }
                    else MessageBox.Show("FromMonth is greater than ToMonth");

                }
                else if (chkEmpName.Checked == true)
                {
                    if (cmbEmpName.SelectedItem == null)
                    {
                        MessageBox.Show("Please select Employee Name");
                        return;
                    }
                    query += @"where employee_name='" + cmbEmpName.Text + "'";
                }
                else
                    {
              
                }

                EmployeeTaxCalculationDataSet ds = new EmployeeTaxCalculationDataSet();

                cmd = new SqlCommand(query, con);
                da = new SqlDataAdapter(cmd);
                da.Fill(ds, ds.Tables[6].TableName);


                ReportDataSource datasource = new ReportDataSource("Tax_Detail_Report", ds.Tables[6]);


                ReportParameter p1 = new ReportParameter("FromDate", sDate);
                ReportParameter p2 = new ReportParameter("ToDate", eDate);
               // this.rpvTaxDetail.LocalReport.SetParameters(new ReportParameter[] { p1, p2 }); 


                this.rpvTaxDetail.LocalReport.DataSources.Clear();
                this.rpvTaxDetail.LocalReport.DataSources.Add(datasource);
                this.rpvTaxDetail.LocalReport.Refresh();


                this.rpvTaxDetail.RefreshReport();
                con.Close();
          
        }
               

       
    }
}
