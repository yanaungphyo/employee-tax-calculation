﻿namespace EmployeeTaxCalculation
{
    partial class frmTaxDetailReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.Tax_Detail_ReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.EmployeeTaxCalculationDataSet = new EmployeeTaxCalculation.EmployeeTaxCalculationDataSet();
            this.chkEmpName = new System.Windows.Forms.CheckBox();
            this.chkMonth = new System.Windows.Forms.CheckBox();
            this.cmbEmpName = new System.Windows.Forms.ComboBox();
            this.dtpMonthFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpMonthTo = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rpvTaxDetail = new Microsoft.Reporting.WinForms.ReportViewer();
            this.Tax_Yearly_ReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Tax_Detail_ReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeTaxCalculationDataSet)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Tax_Yearly_ReportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Tax_Detail_ReportBindingSource
            // 
            this.Tax_Detail_ReportBindingSource.DataMember = "Tax_Detail_Report";
            this.Tax_Detail_ReportBindingSource.DataSource = this.EmployeeTaxCalculationDataSet;
            // 
            // EmployeeTaxCalculationDataSet
            // 
            this.EmployeeTaxCalculationDataSet.DataSetName = "EmployeeTaxCalculationDataSet";
            this.EmployeeTaxCalculationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // chkEmpName
            // 
            this.chkEmpName.AutoSize = true;
            this.chkEmpName.Location = new System.Drawing.Point(8, 14);
            this.chkEmpName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkEmpName.Name = "chkEmpName";
            this.chkEmpName.Size = new System.Drawing.Size(128, 28);
            this.chkEmpName.TabIndex = 0;
            this.chkEmpName.Text = "Employee Name";
            this.chkEmpName.UseVisualStyleBackColor = true;
            // 
            // chkMonth
            // 
            this.chkMonth.AutoSize = true;
            this.chkMonth.Location = new System.Drawing.Point(8, 52);
            this.chkMonth.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.chkMonth.Name = "chkMonth";
            this.chkMonth.Size = new System.Drawing.Size(104, 28);
            this.chkMonth.TabIndex = 1;
            this.chkMonth.Text = "Month From";
            this.chkMonth.UseVisualStyleBackColor = true;
            // 
            // cmbEmpName
            // 
            this.cmbEmpName.FormattingEnabled = true;
            this.cmbEmpName.Location = new System.Drawing.Point(142, 12);
            this.cmbEmpName.Name = "cmbEmpName";
            this.cmbEmpName.Size = new System.Drawing.Size(121, 30);
            this.cmbEmpName.TabIndex = 2;
            // 
            // dtpMonthFrom
            // 
            this.dtpMonthFrom.Cursor = System.Windows.Forms.Cursors.Default;
            this.dtpMonthFrom.CustomFormat = "MMM/yyyy";
            this.dtpMonthFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonthFrom.Location = new System.Drawing.Point(118, 52);
            this.dtpMonthFrom.Name = "dtpMonthFrom";
            this.dtpMonthFrom.ShowUpDown = true;
            this.dtpMonthFrom.Size = new System.Drawing.Size(100, 30);
            this.dtpMonthFrom.TabIndex = 3;
            // 
            // dtpMonthTo
            // 
            this.dtpMonthTo.CustomFormat = "MMM/yyyy";
            this.dtpMonthTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonthTo.Location = new System.Drawing.Point(269, 54);
            this.dtpMonthTo.Name = "dtpMonthTo";
            this.dtpMonthTo.ShowUpDown = true;
            this.dtpMonthTo.Size = new System.Drawing.Size(100, 30);
            this.dtpMonthTo.TabIndex = 4;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(237, 54);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(26, 24);
            this.lblTo.TabIndex = 5;
            this.lblTo.Text = "To";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.dtpMonthTo);
            this.panel1.Controls.Add(this.lblTo);
            this.panel1.Controls.Add(this.chkEmpName);
            this.panel1.Controls.Add(this.chkMonth);
            this.panel1.Controls.Add(this.dtpMonthFrom);
            this.panel1.Controls.Add(this.cmbEmpName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(803, 94);
            this.panel1.TabIndex = 6;
            // 
            // btnSearch
            // 
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.Location = new System.Drawing.Point(392, 54);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(100, 30);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rpvTaxDetail);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 94);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(803, 338);
            this.panel2.TabIndex = 7;
            // 
            // rpvTaxDetail
            // 
            this.rpvTaxDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "Tax_Detail_Report";
            reportDataSource1.Value = this.Tax_Detail_ReportBindingSource;
            this.rpvTaxDetail.LocalReport.DataSources.Add(reportDataSource1);
            this.rpvTaxDetail.LocalReport.ReportEmbeddedResource = "EmployeeTaxCalculation.TaxDetailReport.rdlc";
            this.rpvTaxDetail.Location = new System.Drawing.Point(0, 0);
            this.rpvTaxDetail.Name = "rpvTaxDetail";
            this.rpvTaxDetail.Size = new System.Drawing.Size(803, 338);
            this.rpvTaxDetail.TabIndex = 0;
            // 
            // Tax_Yearly_ReportBindingSource
            // 
            this.Tax_Yearly_ReportBindingSource.DataMember = "Tax_Yearly_Report";
            this.Tax_Yearly_ReportBindingSource.DataSource = this.EmployeeTaxCalculationDataSet;
            // 
            // frmTaxDetailReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 432);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmTaxDetailReport";
            this.Text = "TaxDetailReport";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.TaxDetailReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Tax_Detail_ReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeTaxCalculationDataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Tax_Yearly_ReportBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkEmpName;
        private System.Windows.Forms.CheckBox chkMonth;
        private System.Windows.Forms.ComboBox cmbEmpName;
        private System.Windows.Forms.DateTimePicker dtpMonthFrom;
        private System.Windows.Forms.DateTimePicker dtpMonthTo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.BindingSource Tax_Detail_ReportBindingSource;
        private EmployeeTaxCalculationDataSet EmployeeTaxCalculationDataSet;
        private Microsoft.Reporting.WinForms.ReportViewer rpvTaxDetail;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.BindingSource Tax_Yearly_ReportBindingSource;
    }
}