﻿namespace EmployeeTaxCalculation
{
    partial class frmTaxRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblMiniTaxableIncome = new System.Windows.Forms.Label();
            this.lblMaxTaxRate = new System.Windows.Forms.Label();
            this.lblPercentage = new System.Windows.Forms.Label();
            this.btnTaxRateSave = new System.Windows.Forms.Button();
            this.txtMinTaxableIncome = new System.Windows.Forms.TextBox();
            this.txtMaxTaxableIncome = new System.Windows.Forms.TextBox();
            this.txtPercentage = new System.Windows.Forms.TextBox();
            this.dgvTaxRate = new System.Windows.Forms.DataGridView();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkAbove = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.col_No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_taxrate_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_mintaxRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_maxtaxRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Percentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_above = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTaxRate)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMiniTaxableIncome
            // 
            this.lblMiniTaxableIncome.AutoSize = true;
            this.lblMiniTaxableIncome.Location = new System.Drawing.Point(19, 12);
            this.lblMiniTaxableIncome.Name = "lblMiniTaxableIncome";
            this.lblMiniTaxableIncome.Size = new System.Drawing.Size(166, 24);
            this.lblMiniTaxableIncome.TabIndex = 0;
            this.lblMiniTaxableIncome.Text = "Minimum Taxable Income";
            // 
            // lblMaxTaxRate
            // 
            this.lblMaxTaxRate.AutoSize = true;
            this.lblMaxTaxRate.Location = new System.Drawing.Point(19, 52);
            this.lblMaxTaxRate.Name = "lblMaxTaxRate";
            this.lblMaxTaxRate.Size = new System.Drawing.Size(168, 24);
            this.lblMaxTaxRate.TabIndex = 1;
            this.lblMaxTaxRate.Text = "Maximum Taxable Income";
            // 
            // lblPercentage
            // 
            this.lblPercentage.AutoSize = true;
            this.lblPercentage.Location = new System.Drawing.Point(19, 92);
            this.lblPercentage.Name = "lblPercentage";
            this.lblPercentage.Size = new System.Drawing.Size(77, 24);
            this.lblPercentage.TabIndex = 2;
            this.lblPercentage.Text = "Percentage";
            // 
            // btnTaxRateSave
            // 
            this.btnTaxRateSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTaxRateSave.Location = new System.Drawing.Point(23, 122);
            this.btnTaxRateSave.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnTaxRateSave.Name = "btnTaxRateSave";
            this.btnTaxRateSave.Size = new System.Drawing.Size(100, 40);
            this.btnTaxRateSave.TabIndex = 3;
            this.btnTaxRateSave.Text = "Save";
            this.btnTaxRateSave.UseVisualStyleBackColor = true;
            this.btnTaxRateSave.Click += new System.EventHandler(this.btnTaxRateSave_Click);
            // 
            // txtMinTaxableIncome
            // 
            this.txtMinTaxableIncome.Location = new System.Drawing.Point(191, 6);
            this.txtMinTaxableIncome.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtMinTaxableIncome.Name = "txtMinTaxableIncome";
            this.txtMinTaxableIncome.Size = new System.Drawing.Size(125, 30);
            this.txtMinTaxableIncome.TabIndex = 4;
            // 
            // txtMaxTaxableIncome
            // 
            this.txtMaxTaxableIncome.Location = new System.Drawing.Point(191, 46);
            this.txtMaxTaxableIncome.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtMaxTaxableIncome.Name = "txtMaxTaxableIncome";
            this.txtMaxTaxableIncome.Size = new System.Drawing.Size(125, 30);
            this.txtMaxTaxableIncome.TabIndex = 5;
            // 
            // txtPercentage
            // 
            this.txtPercentage.Location = new System.Drawing.Point(191, 86);
            this.txtPercentage.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtPercentage.Name = "txtPercentage";
            this.txtPercentage.Size = new System.Drawing.Size(125, 30);
            this.txtPercentage.TabIndex = 6;
            // 
            // dgvTaxRate
            // 
            this.dgvTaxRate.AllowUserToAddRows = false;
            this.dgvTaxRate.AllowUserToDeleteRows = false;
            this.dgvTaxRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTaxRate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_No,
            this.col_taxrate_id,
            this.col_mintaxRate,
            this.col_maxtaxRate,
            this.col_Percentage,
            this.col_above});
            this.dgvTaxRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTaxRate.Location = new System.Drawing.Point(0, 0);
            this.dgvTaxRate.MultiSelect = false;
            this.dgvTaxRate.Name = "dgvTaxRate";
            this.dgvTaxRate.ReadOnly = true;
            this.dgvTaxRate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTaxRate.Size = new System.Drawing.Size(762, 403);
            this.dgvTaxRate.TabIndex = 7;
            this.dgvTaxRate.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvTaxRate_RowPostPaint);
            this.dgvTaxRate.DoubleClick += new System.EventHandler(this.dgvTaxRate_DoubleClick);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Location = new System.Drawing.Point(133, 121);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 40);
            this.btnUpdate.TabIndex = 8;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Location = new System.Drawing.Point(239, 121);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 40);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkAbove);
            this.panel1.Controls.Add(this.txtMaxTaxableIncome);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.lblMiniTaxableIncome);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.lblMaxTaxRate);
            this.panel1.Controls.Add(this.lblPercentage);
            this.panel1.Controls.Add(this.txtPercentage);
            this.panel1.Controls.Add(this.btnTaxRateSave);
            this.panel1.Controls.Add(this.txtMinTaxableIncome);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(762, 167);
            this.panel1.TabIndex = 10;
            // 
            // chkAbove
            // 
            this.chkAbove.AutoSize = true;
            this.chkAbove.Location = new System.Drawing.Point(323, 52);
            this.chkAbove.Name = "chkAbove";
            this.chkAbove.Size = new System.Drawing.Size(67, 28);
            this.chkAbove.TabIndex = 10;
            this.chkAbove.Text = "Above";
            this.chkAbove.UseVisualStyleBackColor = true;
            this.chkAbove.CheckedChanged += new System.EventHandler(this.chkAbove_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvTaxRate);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 167);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(762, 403);
            this.panel2.TabIndex = 11;
            // 
            // col_No
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.col_No.DefaultCellStyle = dataGridViewCellStyle1;
            this.col_No.HeaderText = "No";
            this.col_No.Name = "col_No";
            this.col_No.ReadOnly = true;
            // 
            // col_taxrate_id
            // 
            this.col_taxrate_id.DataPropertyName = "id";
            this.col_taxrate_id.HeaderText = "Tax Rate id";
            this.col_taxrate_id.Name = "col_taxrate_id";
            this.col_taxrate_id.ReadOnly = true;
            this.col_taxrate_id.Visible = false;
            // 
            // col_mintaxRate
            // 
            this.col_mintaxRate.DataPropertyName = "min_taxable_income";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_mintaxRate.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_mintaxRate.HeaderText = "Minimum Tax Rate";
            this.col_mintaxRate.Name = "col_mintaxRate";
            this.col_mintaxRate.ReadOnly = true;
            // 
            // col_maxtaxRate
            // 
            this.col_maxtaxRate.DataPropertyName = "max_taxable_income";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.col_maxtaxRate.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_maxtaxRate.HeaderText = "Maximum Tax Rate";
            this.col_maxtaxRate.Name = "col_maxtaxRate";
            this.col_maxtaxRate.ReadOnly = true;
            // 
            // col_Percentage
            // 
            this.col_Percentage.DataPropertyName = "percentage";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.col_Percentage.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_Percentage.HeaderText = "Percentage";
            this.col_Percentage.Name = "col_Percentage";
            this.col_Percentage.ReadOnly = true;
            // 
            // col_above
            // 
            this.col_above.DataPropertyName = "above";
            this.col_above.HeaderText = "Above";
            this.col_above.Name = "col_above";
            this.col_above.ReadOnly = true;
            this.col_above.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.col_above.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // frmTaxRate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 570);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Pyidaungsu", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "frmTaxRate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TaxRate";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTaxRate)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMiniTaxableIncome;
        private System.Windows.Forms.Label lblMaxTaxRate;
        private System.Windows.Forms.Label lblPercentage;
        private System.Windows.Forms.Button btnTaxRateSave;
        private System.Windows.Forms.TextBox txtMinTaxableIncome;
        private System.Windows.Forms.TextBox txtMaxTaxableIncome;
        private System.Windows.Forms.TextBox txtPercentage;
        private System.Windows.Forms.DataGridView dgvTaxRate;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkAbove;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_No;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_taxrate_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_mintaxRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_maxtaxRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Percentage;
        private System.Windows.Forms.DataGridViewCheckBoxColumn col_above;
    }
}