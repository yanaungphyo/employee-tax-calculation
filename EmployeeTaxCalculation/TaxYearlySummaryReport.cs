﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace EmployeeTaxCalculation
{
    public partial class frmTaxYearlySummaryReport : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        public frmTaxYearlySummaryReport()
        {
            InitializeComponent();
        }

        private void TaxYearlySummaryReport_Load(object sender, EventArgs e)
        {
            dtpFromYear.Value = DateTime.Today;
            dtpToYear.Value = DateTime.Today;
            string querySelect = "select id,employee_name from Employee where is_deleted=0";
            cmd = new SqlCommand(querySelect, con);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);

            cmbEmpName.DisplayMember = "employee_name";
            cmbEmpName.ValueMember = "id";
            cmbEmpName.DataSource = dt;
            this.cmbEmpName.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cmbEmpName.AutoCompleteSource = AutoCompleteSource.ListItems;
            

            
            this.rpvTaxYearly.RefreshReport();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)
            con.Open();
            
                string sDate = dtpFromYear.Value.ToString("yyyy");
                string eDate = dtpToYear.Value.ToString("yyyy");
                string query = @"select * from vw_yearly_report ";
                if (chkEmpName.Checked == true & chkYear.Checked == true)
                {
                    if (cmbEmpName.SelectedItem == null)
                    {
                        MessageBox.Show("Please select Employee Name");
                        return;
                    }
                    if (dtpToYear.Value >= dtpFromYear.Value)
                    {
                        query += @"where employee_name='" + cmbEmpName.Text + "'" + " and (taxable_year between " + dtpFromYear.Value.Year + " and " + dtpToYear.Value.Year + ") ";
                    }
                    else MessageBox.Show("FromYear is greater than ToYear");
                    }
                else if (chkYear.Checked == true)
                {
                    if (dtpToYear.Value >= dtpFromYear.Value)
                    {
                        query += @"where (taxable_year between " + dtpFromYear.Value.Year + " and " + dtpToYear.Value.Year + ") ";
                    }
                    else MessageBox.Show("FromYear is greater than ToYear");
                }
                else if (chkEmpName.Checked == true)
                {
                    if (cmbEmpName.SelectedItem == null)
                    {
                        MessageBox.Show("Please select Employee Name");
                        return;
                    }
                    query += @"where employee_name='" + cmbEmpName.Text + "'";
                }
                else
                {

                }

                EmployeeTaxCalculationDataSet ds = new EmployeeTaxCalculationDataSet();

                cmd = new SqlCommand(query, con);
                da = new SqlDataAdapter(cmd);
                da.Fill(ds, ds.Tables[7].TableName);


                ReportParameter p1 = new ReportParameter("FromDate", sDate);
                ReportParameter p2 = new ReportParameter("ToDate", eDate);
                this.rpvTaxYearly.LocalReport.SetParameters(new ReportParameter[] { p1, p2 }); 

                ReportDataSource datasource = new ReportDataSource("Tax_Yearly_Report", ds.Tables[7]);
                this.rpvTaxYearly.LocalReport.DataSources.Clear();
                this.rpvTaxYearly.LocalReport.DataSources.Add(datasource);
                this.rpvTaxYearly.LocalReport.Refresh();


                this.rpvTaxYearly.RefreshReport();
                con.Close();
           
        }
    }
}
