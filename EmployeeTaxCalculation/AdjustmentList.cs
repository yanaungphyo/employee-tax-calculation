﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmAdjustmentList : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        public frmAdjustmentList()
        {
            InitializeComponent();
            dgvTaxableYear.AutoGenerateColumns = false;
            RefreshData();
        }

        private void RefreshData()
        {
            con.Open();
            
            cmd = new SqlCommand(@"select y.id,employee_name,adjustment_date, taxable_year, total_tax_amount, 
adjustment_amount from Taxable_Yearly_Payment as y inner join Employee as e on y.employee_id=e.id where e.is_deleted=0 and y.is_deleted=0", con);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);
            dgvTaxableYear.DataSource = dt;
            con.Close();
        }
                
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmAdjustmentRegister frmAdjReg = new frmAdjustmentRegister();
            frmAdjReg.btnUpdate.Enabled = false;
            frmAdjReg.ShowDialog();
            
            RefreshData();
        }

        private void frmAdjustmentList_Load(object sender, EventArgs e)
        {
            RefreshData();
            con.Open();            
            dtpFromYear.Value = new DateTime(2019, 1, 1);
            dtpToYear.Value = new DateTime(2019, 1, 1);            
            string querySelect = "select id,employee_name from Employee where is_deleted=0";
            cmd = new SqlCommand(querySelect, con);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);

            cmbEmpName.DisplayMember = "employee_name";
            cmbEmpName.ValueMember = "id";
            cmbEmpName.DataSource = dt;
            this.cmbEmpName.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cmbEmpName.AutoCompleteSource = AutoCompleteSource.ListItems;
            con.Close();
            
        }

        

        private void dgvTaxableYear_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dgvTaxableYear.Rows[e.RowIndex].Cells["col_no"].Value = e.RowIndex + 1;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if(con.State!=ConnectionState.Open)

            con.Open();
           
                string Query = @"select y.id,employee_name,adjustment_date, taxable_year, total_tax_amount, 
                adjustment_amount from Taxable_Yearly_Payment as y inner join Employee as e on y.employee_id=e.id where e.is_deleted=0 and y.is_deleted=0";
                if (chkEmpName.Checked == true & chkYear.Checked == true)
                {
                    if (cmbEmpName.SelectedItem == null)
                    {
                        MessageBox.Show("Please select correct Emploee Name");
                        return;
                    }
                    if (dtpToYear.Value >= dtpFromYear.Value)
                    {
                        Query += " and employee_id=" + cmbEmpName.SelectedValue + "and (taxable_year between " + dtpFromYear.Value.Year + "and " + dtpFromYear.Value.Year + ") ";
                        cmd = new SqlCommand(Query, con);
                        da = new SqlDataAdapter(cmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        dgvTaxableYear.DataSource = dt;
                    }
                    else MessageBox.Show("FromYear is greater than ToYear");
                }
                else if (chkYear.Checked == true)
                {
                    if (dtpToYear.Value >= dtpFromYear.Value)
                    {
                        Query += @"  and (taxable_year between " + dtpFromYear.Value.Year + "and " + dtpToYear.Value.Year + ")";
                        cmd = new SqlCommand(Query, con);
                        da = new SqlDataAdapter(cmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        dgvTaxableYear.DataSource = dt;
                    }
                    else
                        MessageBox.Show("FromYear is greater than ToYear");

                }
                else if (chkEmpName.Checked == true)
                {
                    if (cmbEmpName.SelectedItem == null)
                    {
                        MessageBox.Show("Please select correct Emploee Name");
                        return;
                    }
                    Query += " and employee_id= " + cmbEmpName.SelectedValue;
                    cmd = new SqlCommand(Query, con);
                    da = new SqlDataAdapter(cmd);
                    dt = new DataTable();
                    da.Fill(dt);
                    dgvTaxableYear.DataSource = dt;
                }
                else
                {
                    cmd = new SqlCommand(Query, con);
                    da = new SqlDataAdapter(cmd);
                    dt = new DataTable();
                    da.Fill(dt);
                    dgvTaxableYear.DataSource = dt;
                }

                con.Close();
               
            }
            
        

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            con.Open();

            int ID = int.Parse(dgvTaxableYear.SelectedRows[0].Cells["col_id"].Value.ToString());
            frmAdjustmentRegister frmAdjReg = new frmAdjustmentRegister(ID);

            frmAdjReg.btnSave.Enabled = false;
            frmAdjReg.ShowDialog();
            con.Close();
            RefreshData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            con.Open();
            try
            {
                DialogResult result = MessageBox.Show("Are you sure you want to delete?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    int ID = int.Parse(dgvTaxableYear.SelectedRows[0].Cells["col_id"].Value.ToString());
                    cmd = new SqlCommand("UPDATE Taxable_Yearly_Payment SET  is_deleted=1 where id=@id", con);
                    cmd.Parameters.AddWithValue("@id", ID);
                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Deleted Successfully!");

                }
                con.Close();
                RefreshData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please select Employee");
            }
        }
    }
}
