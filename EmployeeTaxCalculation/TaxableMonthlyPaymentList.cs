﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace EmployeeTaxCalculation
{
    public partial class frmTaxableMonthlyPaymentList : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["EmployeeTaxCalculation.Properties.Settings.EmployeeTaxCalculationConnectionString"].ConnectionString);
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;
        
        public frmTaxableMonthlyPaymentList()
        {
            InitializeComponent();
            dgvMonthlyList.AutoGenerateColumns = false;
            refrshData();
        }

        private void refrshData()
        {
            con.Open();
            this.chkEmpName.Checked = false;
            this.chkMonth.Checked = false;
            
            DataTable dt = new DataTable();
            da = new SqlDataAdapter("select M.id,employee_name, taxable_month, taxable_year, monthly_salary, annual_salary, personal_relief, parents_relief, spouse_relief, child_relief, other_deductible, taxable_income, total_tax,monthly_tax from Taxable_Monthly_Payment As M join Employee As E on E.id=M.employee_id  where E.is_deleted=0 and M.is_deleted=0 ", con);
            da.Fill(dt);
            dgvMonthlyList.DataSource = dt;
            con.Close();
        }

        private void frmTaxableMonthlyPaymentList_Load(object sender, EventArgs e)
        {
            con.Open();
            string querySelect = "select id,employee_name from Employee where is_deleted=0";
            cmd = new SqlCommand(querySelect, con);
            da = new SqlDataAdapter(cmd);
            dt = new DataTable();
            da.Fill(dt);

            cmbEmpName.DisplayMember = "employee_name";
            cmbEmpName.ValueMember = "id";
            cmbEmpName.DataSource = dt;
            this.cmbEmpName.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cmbEmpName.AutoCompleteSource = AutoCompleteSource.ListItems;
            con.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            if(con.State !=ConnectionState.Open)
            con.Open();
            
            
                string Query = @"select M.id,employee_name, taxable_month, taxable_year, monthly_salary, annual_salary, personal_relief, parents_relief, spouse_relief, child_relief, other_deductible, taxable_income, total_tax,monthly_tax from Taxable_Monthly_Payment As M join Employee As E on E.id=M.employee_id  where E.is_deleted=0 and M.is_deleted=0 ";
                if (chkEmpName.Checked == true & chkMonth.Checked == true)
                {
                    if (cmbEmpName.SelectedItem == null)
                    {
                        MessageBox.Show("Please select correct Emploee Name");
                        return;
                    }
                    if (dtpToMonth.Value >= dtpFromMonth.Value)
                    {
                        Query += " and employee_id=" + cmbEmpName.SelectedValue + " and (taxable_month between " + dtpFromMonth.Value.Month + "and " + dtpToMonth.Value.Month + ") and (taxable_year between " + dtpFromMonth.Value.Year + "and " + dtpToMonth.Value.Year + ") ";
                        cmd = new SqlCommand(Query, con);
                        da = new SqlDataAdapter(cmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        dgvMonthlyList.DataSource = dt;
                    }
                    else MessageBox.Show("FromMonth is greater than ToMonth");
                }
                else if (chkMonth.Checked == true)
                {
                    if (dtpToMonth.Value >= dtpFromMonth.Value)
                    {
                        Query += @"  and (taxable_month between " + dtpFromMonth.Value.Month + "and " + dtpToMonth.Value.Month + ") and (taxable_year between " + dtpFromMonth.Value.Year + "and " + dtpToMonth.Value.Year + ") ";
                        cmd = new SqlCommand(Query, con);
                        da = new SqlDataAdapter(cmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        dgvMonthlyList.DataSource = dt;
                    }
                    else MessageBox.Show("FromMonth is greater than ToMonth");

                }
                else if (chkEmpName.Checked == true)
                {
                    if (cmbEmpName.SelectedItem == null)
                    {
                        MessageBox.Show("Please select correct Emploee Name");
                        return;
                    }
                    Query += " and employee_id=" + cmbEmpName.SelectedValue;
                    cmd = new SqlCommand(Query, con);
                    da = new SqlDataAdapter(cmd);
                    dt = new DataTable();
                    da.Fill(dt);
                    dgvMonthlyList.DataSource = dt;
                }
                else
                {

                    cmd = new SqlCommand(Query, con);
                    da = new SqlDataAdapter(cmd);
                    dt = new DataTable();
                    da.Fill(dt);
                    dgvMonthlyList.DataSource = dt;
                }

                con.Close();
            
        }

        private void dgvMonthlyList_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            dgvMonthlyList.Rows[e.RowIndex].Cells["col_no"].Value = e.RowIndex + 1;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            con.Open();

            DialogResult result = MessageBox.Show("Are you sure you want to delete?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                int ID = int.Parse(dgvMonthlyList.SelectedRows[0].Cells["col_id"].Value.ToString());
                cmd = new SqlCommand("UPDATE Taxable_Monthly_Payment SET  is_deleted=1 where id=@id", con);
                cmd.Parameters.AddWithValue("@id", ID);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Deleted Successfully!");

            }
            con.Close();
            refrshData();
        }
   
        
    }
}
